/*
 ============================================================================
 Name        : Lab01.c
 Created on  : Mar 10, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that takes 2 input numbers i.e. x and y; and
 do the following:
 + Check value of given x is less that a predefined value
 + Check value of given y is less that a predefined value
 + Print the result of x + y
 + Print the result of x * y
 + Print the result of (x + y) * x
 + Use Macros to hold the predefined values of x and y
 + Use Macro to hold addition formula
 + Use Macro to hold multiplication formula
 ============================================================================
PSEUDOCODE:
print request for input
        "Enter x value : "
        "Enter y value : "
READ x, y
vX = 0
vY = 0
z = 1000
IF x < z THEN
    print response
        "X value(" + x + ") is less than " + z
ELSE
    print response
        "X value(" + x + ") is greater than " + z
ENDIF

IF y < z THEN
    print response
        "Y value(" + y + ") is less than " + z
ELSE
    print response
        "Y value(" + y + ") is greater than " + z
ENDIF

SET vX = x
SET vY = y

print response
    "The result of x + y = " + (vX + vY)
print response
    "nThe result of x * y = " + (vX * vY)
print response
    "The result of (x + y) * x = " + ((vX + vY) * vX)
*/

#include <stdio.h>

int z = 1000;
int vX, vY;
#define CheckX(x) x < z ? 1 : 0
#define CheckY(y) y < z ? 1 : 0
#define SetX(m) vX = m
#define SetY(m) vY = m
#define Add() vX + vY
#define Multi() vX * vY
#define Equation() (vX + vY) * vY

int main(void)
{
    int x, y;
    printf("Enter x value : ");
    scanf("%d", &x);
    printf("Enter y value : ");
    scanf("%d", &y);
    if(CheckX(x))
        printf("\nX value(%d) is less than %d", x, z);
    else
        printf("\nX value(%d) is greater than or equal %d", x, z);
    if(CheckY(y))
        printf("\nY value(%d) is less than %d", y, z);
    else
        printf("\nY value(%d) is greater than or equal %d", y, z);

    SetX(x);
    SetY(y);
    printf("\nThe result of x + y = %d", Add());
    printf("\nThe result of x * y = %d", Multi());
    printf("\nThe result of (x + y) * x = %d", Equation());
	return 0;
}
