/*
 ============================================================================
 Name        : Lab03.c
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Run the following program and see if the result is as expected and
 if not � debug until it works properly
 ============================================================================

 PSEUDOCODE:
 index = 0
 sum = 0
 WHILE index <= 1000
    sum = sum + index
    ADD 1 to index
 ENDWHILE
 WRITE "The sum of the integers from 1 to 1000 is " + sum
*/

#include <stdio.h>
/* Print the sum of the integers from 1 to 1000 */
int main(int argc, char **argv)
{
	int sum=0;
	int index;
	for(index=0; index <= 1000; index++)
        sum += index;
    printf("The sum of the integers from 1 to 1000 is %d", sum);
	return 0;
}
