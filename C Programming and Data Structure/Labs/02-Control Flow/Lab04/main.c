/*
 ============================================================================
 Name        : Lab04.c
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- takes an input value from the user, and
    2- prints the summation result of the odd the numbers, from 0 to the
 given input value

    3- * Use �for� statement
 ============================================================================

 PSEUDOCODE:
 print request for input
        "Enter a nubmer to get summation of the odd numbers : "
 READ givenValue
 index = 0
 sum = 0
 WHILE index <= givenValue
    IF index % 2 <> 0
        sum = sum + index
        ADD 1 to index
    ENDIF
 ENDWHILE
 WRITE "The summation of the odd numbers is : " + sum
*/

#include <stdio.h>

int main(void)
{
	int givenValue;
	int sum = 0;
	printf("Enter a nubmer to get summation of the odd numbers : ");
	scanf("%d", & givenValue);
	for(int index=0; index <= givenValue; index++)
    {
        if(index % 2 != 0)
            sum += index;
    }
	printf("\nThe summation of the odd numbers is : %d", sum);
	return 0;
}
