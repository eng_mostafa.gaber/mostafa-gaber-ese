/*
 ============================================================================
 Name        : Lab05.c
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- takes an input value from the user, and
    2- prints the factorial value
    3- * Use �while� statement
 ============================================================================

 PSEUDOCODE:
 print request for input
        "Enter a nubmer to get factorial value : "
 READ givenValue
 index = 0
 sum = 0
 WHILE index <= givenValue
    sum = sum * index
    ADD 1 to index
 ENDWHILE
 WRITE "The factorial value of the number " + givenValue + " is : " + sum
*/

#include <stdio.h>

int main(void)
{
	int givenValue;
	double sum = 1;
	int index=1;
	printf("Enter a nubmer to get factorial value : ");
	scanf("%d", & givenValue);
	while(index <= givenValue)
    {
        sum *= index;
        index++;
    }
	printf("\nThe factorial value of the number %d is : %lf", givenValue, sum);
	return 0;
}
