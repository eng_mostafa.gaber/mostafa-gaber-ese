/*
 ============================================================================
 Name        : Lab02.c
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that takes the grade of a student ranging
 from 0 to 100; and prints the grade as Excellent, Very Good,
 Good, Pass, Fail
 * Use �switch� statement
 ============================================================================
PSEUDOCODE:
Input a Grade

Case based on Grade
    Case >= 90 And <= 100
        Report "You are Excellent :)"
    Case >= 80 And <= 89.99
        Report "You are Very Good :)"
    Case >= 70 And <= 79.99
        Report "You are Good :)"
    Case >= 50 And <= 69.99
        Report "You are Pass :("
    Case < 50
        Report "You are Fail :("
    Default
        Report "Present a valid grade!!!"
End Case
*/

#include <stdio.h>

int main (void)
{
    int grade;
    printf("Enter your grade : ");
    scanf("%f", &grade);
    switch(grade){
    case >= 90 && <= 100://://
    //case 90: case 91: case 92: case 93: case 94:
    //case 95: case 96: case 97: case 98: case 99:
    //case 100:
        printf("You are Excellent :%c", ')');
        break;
    case >= 80 && <= 89:
    //case 80: case 81: case 82: case 83: case 84:
    //case 85: case 86: case 87: case 88: case 89:
        printf("You are Very Good :%c", ')');
        break;
    case >= 70 && <= 79:
    //case 70: case 71: case 72: case 73: case 74:
    //case 75: case 76: case 77: case 78: case 79:
        printf("You are Good :%c", ')');
        break;
    case >= 50 && <= 69:
    //case 50: case 51: case 52: case 53: case 54:
    //case 55: case 56: case 57: case 58: case 59:
    //case 60: case 61: case 62: case 63: case 64:
    //case 65: case 66: case 67: case 68: case 69:
        printf("You are pass %c:", ')');
        break;
    case < 50
        printf("You are fail %c:", ')');
        break;
    default:
        printf("Present a valid grade!!!");
        break;
    }
	return 0;
}
