/*
 ============================================================================
 Name        : MacroAddFun.c
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Define add function as a macor
    3- Print the result.

 Pesdocode:
    1- Define add function as a macor
    3- Print the result.
 ============================================================================
*/
#include <stdio.h>
#define add(x, y) x + y

int main()
{
    int results = 0;
    results = add(5, 10);
    printf("The result of two numbers is %d", results);
    return 0;
}
