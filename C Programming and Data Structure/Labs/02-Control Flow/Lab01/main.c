/*
 ============================================================================
 Name        : Lab01.c
 Created on  : Mar 2, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that takes the grade of a student ranging
 from 0 to 100; and prints the grade as Excellent, Very Good,
 Good, Pass, Fail
 * Use �if � statements
 ============================================================================
PSEUDOCODE:
print request for input
        "Enter your grade : "

if >= 90 And <= 100
    print response
        "You are Excellent :)"
if >= 80 And <= 89.99
    print response
        "You are Very Good :)"
if >= 70 And <= 79.99
    print response
        "You are Good :)"
if >= 50 And <= 69.99
    print response
        "You are Pass :("
if < 50
    print response
        "You are Fail :("

if input isn't recognized
    print response
        "Present a valid grade!!!"
*/


#include <stdio.h>

int main()
{
    float grade;
    printf("Enter your grade : ");
    scanf("%f", &grade);
    if(grade >= 90 && grade <= 100)
        //printf("You are Excellent :\)");
        printf("You are Excellent :%c", ')');
    else if(grade >= 80 && grade <= 89.99)
        printf("You are Very Good :%c", ')');
    else if(grade >= 70 && grade <= 79.99)
        printf("You are Good :%c", ')');
    else if(grade >= 50 && grade <= 69.99)
        printf("You are pass %c:", ')');
    else if(grade < 50)
        printf("You are fail %c:", ')');
    else
        printf("Present a valid grade!!!");
    return 0;
}
