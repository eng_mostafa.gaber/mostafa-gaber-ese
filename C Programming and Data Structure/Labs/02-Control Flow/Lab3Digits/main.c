/*
 ============================================================================
 Name        : Lab3Digits.c
 Author      : Khaled Naga
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- takes an input value from the user in 3 digits
    2- Invers the 3 digits
    3- Print the invers number.

 Pesdocode:
    1- Define varibale to takes an input value from the user.
    2- Check that the digits length not greater than 3 digits.
    3- Invers the 3 digits
    3- Print the invers number.
 ============================================================================
*/
#include <stdio.h>
#include <stdlib.h>

int main()
{

	int givenValue;
	int sum = 0;
	int ones=0;
	int tens=0;
	int handreds=0;
	while(1){
        printf("Enter a nubmer to less than thousand (1000) : ");
        scanf("%d", & givenValue);
        if(givenValue>100 && givenValue < 1000){
            ones = givenValue % 10;
            tens = givenValue % 100;
            handreds = givenValue % 1000;
            //handreds = handreds - ones - (tens * 10);
            tens -= ones;
            tens /= 10;

            /*ones = givenValue % 10;
            tens = (givenValue / 10) % 10;
            handreds = givenValue / 100;
            sum = (ones * 100) + (tens * 10) + handreds;*/
            //printf("Then invert value of %d is %d", givenValue, sum);
            printf("\nThe ones is %d, the Tens is %d, the Handreds is %d", ones, tens, handreds);

            break;
        }
	}
    return 0;
}
