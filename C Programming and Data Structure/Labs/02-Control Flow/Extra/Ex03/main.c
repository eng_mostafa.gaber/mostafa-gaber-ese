/*
 * Ex09.c
 * Session 2: Loops and decisions
 *  Created on: Mar 5, 2019
 *      Author: Mostafa Abuelnour Gaber
 */
// purpose: This program is part of C Course On AMIT Learning

/* *******************************************************
 * Write a C code that check whether a number is prime
 * *****************output sample**************************
 * Enter a number: 3
 * 3 is prime
 *
 * Enter a number: 4
 * 4 is divisible by 2
 * ********************************************************
 */
/* Notes: please refer to >> https://en.wikipedia.org/wiki/Prime_number
 * if you don't remember what's prime number
 * ********************************************************

 PSEUDOCODE:
 index = 2
 Divisible = 0
 print request for input
        "Enter a number between 2 and 999: "
 READ givenValue
 IF givenValue > 1 AND givenValue < 1000
     WHILE index <= 9
        IF givenValue % index = 0 AND givenValue <> index
            Divisible = index
            BREAK
        ENDIF
        ADD 1 to index
     ENDWHILE

     IF Divisible = 0
            WRITE givenValue + "is prime"
     ELSE
            WRITE givenValue + " is divisible by " + Divisible
     ENDIF
 ELSE
    WRITE givenValue + " is not applicable!!!"
 ENDIF
 */
#include<stdio.h>


int main(void){
    int index = 2, Divisible = 0, givenValue=0;
    printf("Enter a number between 2 and 999: ");
    scanf("%d", &givenValue);
    if(givenValue > 1 && givenValue < 1000){
        while(index <= 9)
        {
            if(givenValue % index == 0 && givenValue != index)
            {
                Divisible = index;
                break;
            }
            index++;
        }
        if(Divisible == 0)
            printf("%d is prime", givenValue);
        else
            printf("%d is divisible by %d", givenValue, Divisible);
    }
    else
        printf("%d is not applicable!!!", givenValue);
    return 0;
}
