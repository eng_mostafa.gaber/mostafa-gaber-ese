/*
 * Ex05.c
 * Session 2: Loops and decisions
 *  Created on: Mar 5, 2019
 *      Author: Mostafa Abuelnour Gaber
 */
// purpose: This program is part of C Course On AMIT Learning

/* *******************************************************
 * Write a C code that finds the largest and the smallest of
 * four integers entered by the user
 * *****************output sample**************************
 * Enter four integers: 21 43 10 35
 * Largest: 43
 * Smallest: 10
 * ********************************************************

 PSEUDOCODE:
 DECLARE maxArray : ARRAY[Size of 4]
 index = 0
 Largest = 0
 Smallest = 0
 print request for input
        "Enter four integers, please press ENTER after each number: "
 WHILE index < 4
    READ maxArray[index] = input value
    ADD 1 to index
 ENDWHILE

 index = 0
 Smallest = maxArray[0]
 WHILE index < 4
    IF Largest < maxArray[index]
        Largest = maxArray[index]
    ENDIF
    IF Smallest > maxArray[index]
        Smallest = maxArray[index]
    ENDIF
    ADD 1 to index
 ENDWHILE
 WRITE "Largest: " + Largest
 WRITE "Smallest: " + Smallest
 */

#include<stdio.h>

int main(void){

    int maxArray[4];
    int index = 0, Largest = 0, Smallest = 0;
    printf("Enter four integers, please press ENTER after each number: ", Largest);
    while(index < 4)
    {
        scanf("%d", &maxArray[index]);
        index++;
    }
    index = 0;
    Smallest = maxArray[0];
    while(index < 4)
    {
        if(Largest < maxArray[index])
            Largest = maxArray[index];
        if(Smallest > maxArray[index])
            Smallest = maxArray[index];
        index++;
    }
    printf("Largest: %d", Largest);
    printf("\nSmallest: %d", Smallest);
return 0;
}
