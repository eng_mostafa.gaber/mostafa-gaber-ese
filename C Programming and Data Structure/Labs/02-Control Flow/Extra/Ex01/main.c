/*
 * Ex02.c
 * Session 2: Loops and decisions
 *  Created on: Mar 5, 2019
 *      Author: Mostafa Abuelnour Gaber
 */
// purpose: This program is part of C Course On AMIT Learning

/* *******************************************************
 * Write a C code that asks the user to enter a three digits
 * number then prints the number with its digits reversed
 * *****************output sample**************************
 * Enter a two digit number: 287
 * The reversal is: 782
 * ********************************************************

 PSEUDOCODE:
 print request for input
        "Enter a three digit number: : "
 READ givenValue
 ones = 0
 tens = 0
 handreds = 0
 IF givenValue < 1000
    ones = givenValue % 10
    tens = (givenValue - ones) % 100
    handreds = (givenValue - ones - tens)/ 100
 ENDIF
 WRITE "The reversal is: " + (ones * 100) + tens + handreds
 */

#include <stdio.h>

int main (void){
	int givenValue;
	int ones = 0, tens = 0, handreds = 0;
	printf("Enter a three digit number: ");
	scanf("%d", & givenValue);
	if(givenValue < 1000)
    {
        ones = givenValue % 10;
        tens = (givenValue - ones) % 100;
        handreds = (givenValue - ones - tens)/ 100;
    }
	printf("\nThe reversal is: %d", (ones * 100) + tens + handreds);
return 0;
}
