/*
 ============================================================================
 Name        : SumArrayElements.c
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Define / Declare array = {3, 5, 8}
    2- Use for to add elements of array.
    3- Print the result.

 Pesdocode:
    1- Define / Declare array = {3, 5, 8}
    2- Use for to add elements of array.
    3- Print the result.
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int arrA[3] = {3, 5, 8};
    int sum = 0;
    for(int index=0; index<3; index++)
    {
        sum += arrA[index];
    }
    printf("The result of add elements of array is %d", sum);
    return 0;
}

