#include <stdio.h>
#include <stdlib.h>
#include "linersearch.h"

int main()
{
    int x = FillArray();
    int y = LinerSearch();
    if(y)
        printf("Yes: your key founded in index %d\n", y + 1);
    return 0;
}
