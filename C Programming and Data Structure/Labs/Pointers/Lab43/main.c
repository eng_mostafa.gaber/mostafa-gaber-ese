
#include <stdio.h>
#include "machine.h"
//#include <stdlib.h>

extern float step = 0.3;

int main()
{
    void (*PtrPrintStep)(float);

    PtrPrintStep = PrintStep;

    (*PtrPrintStep) (step);

    //printf("The value of step is %f", step);
    return 0;
}
