#include <stdio.h>

typedef unsigned int uint8;
/*
uint8 func(uint8 (*B)[10]){
    //(*B)[0] = 5;
    (*B)[1] = 11;
    printf("b[-%d] -1 = %d\n\n", 1, (*B)[1]);
    (*B)[2] = 33;
    (*B)[3] = 55;
    (*B)[9] = 44;
}
*/
uint8 funcDD(uint8 (*B)[10]){
    //(*B)[0] = 5;
    uint8 temp = 11;
    B[1] = &temp;
    printf("DD b[-%d] -1 = %d\n\n", 1, (*B)[1]);
    temp = 33;
    B[2] = &temp;
    temp = 44;
    B[9] = &temp;
}
uint8 func(uint8 (*B)[10]){
    uint8 temp;
    funcDD(&B);
    //(*B)[0] = 5;
    printf("b[-%d] -1 = %d\n\n", 1, (*B)[1]);
    temp = 55;
    B[3] = &temp;
}

int main(void){

    uint8 B[10] = {0};
    B[0] = 2000;
    printf("b[0] = %d\n\n", B[0]);
    func(&B);
    for(int i=1; i <= 10; i++)
    {
        printf("b[%d] = %d\n\n", i, B[i - 1]);
    }
    return 0;
}
