#include <stdio.h>
#include <string.h>
//#include <stdlib.h>

typedef struct {
    char name[20];
    char age;
    int salary;
} emp;

void task(emp *emp1);

int main()
{
    int empCount;
    printf("Enter number of Employees: ");
    scanf("%d", &empCount);
    emp empEntity[empCount];
    emp *emp_master;

    //char vName[20];
    //char vAge;
    //int vSalary;

    for(int index=0; index<empCount; index++)
    {
        printf("Enter Employee Name: ");
        scanf(" %[^\t\n]s", &empEntity[index].name);
        /*scanf(" %[^\t\n]s", &vName);
        strcpy(empEntity[index].name, vName);*/
        printf("Enter Employee Age: ");
        scanf("%d", &empEntity[index].age);
        printf("Enter Employee Salary: ");
        scanf("%d", &empEntity[index].salary);
    }

    for(int index=0; index<empCount; index++)
    {
        emp_master = &empEntity[index];
        task(emp_master);
    }
    return 0;
}

void task(emp *emp1)
{
    printf("%s\n", emp1->name);
    printf("%d\n", emp1->age);
    printf("%d\n", emp1->salary);
}
