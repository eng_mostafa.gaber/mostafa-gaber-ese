#ifndef DBLLINKEDLIST_H_INCLUDED
#define DBLLINKEDLIST_H_INCLUDED

//#define NULL ((void*)0)
enum Display {FirstEntry=1, AfterDelete};

typedef enum Display myDisplay;

myDisplay currDisplay;

struct node
{
    int data;
    struct node *pNext;
    struct node *pPrev;
};

struct node* createnode(int data);
int search(int data);
//void printall(int m);
void printall(myDisplay getDisplay);
int addnode(int data);
int insert(int data, int loc);
int delete(int loc);
int DelListDeleteFirstNode();
int DelListDeleteLastNode();

#endif // DBLLINKEDLIST_H_INCLUDED
