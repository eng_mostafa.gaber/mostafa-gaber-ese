#include <stdio.h>
#include <stdlib.h>
#include "DblLinkedList.h"

int main()
{
    int num,data,insPlc,swch, menu;
    int Counter = 0;
	printf("\n\n Doubly Linked List : Delete node from any position of a doubly linked list :\n");
	printf("----------------------------------------------------------------------------------\n");

    /*printf(" Input the number of nodes (3 or more ): ");
    scanf("%d", &num);
    if(num >= 1)
    {
        for(int i = 1; i <= num; i++)
        {
            printf(" Input data for node %d : ", i);
            scanf("%d", &data);
            addnode(data);
        }
    }
    swch=1;
    printall(swch);
    printf(" Input the position ( 1 to %d ) to delete a node : ", num);
    scanf("%d", &insPlc);

    if(insPlc < 1 || insPlc > num)
    {
        printf("\n Invalid position. Try again.\n ");
    }
    if(insPlc >= 1 && insPlc <= num)
    {
        delete(insPlc);
        swch=2;
        printall(swch);
    }*/
    while(1)
    {
        printf("\n Enter 1- Add new, 2- Update..., 3- Search for..., 4- Delete Node..., 5- List All, 6- Exit\n");
        scanf("%d", &menu);
        switch(menu)
        {
        case 1:
            printf(" Input data for node %d : ", ++Counter);
            scanf("%d", &data);
            addnode(data);
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            //swch=1;
            //printall(swch);
            printall(FirstEntry);
            printf(" Input the position ( 1 to %d ) to delete a node : ", Counter);
            scanf("%d", &insPlc);

            if(insPlc < 1 || insPlc > Counter)
            {
                printf("\n Invalid position. Try again.\n ");
            }
            if(insPlc >= 1 && insPlc <= Counter)
            {
                delete(insPlc);
                //swch=2;
                //printall(swch);
                printall(AfterDelete);
                Counter--;
            }
            break;
        case 5:
            break;
        case 6:
            for(int i=Counter; i > 0; i--)
                delete(i);
            return 0;
            break;
        }
    }
    return 0;
}
