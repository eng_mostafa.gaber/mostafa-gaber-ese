#include <stdlib.h>
#include "DblLinkedList.h"

struct node *pHead;
struct node *pTail;
/*
typedef enum Display myDisplay;

myDisplay currDisplay;*/
struct node* createnode(int data)
{
    struct node *ptr=NULL;
    ptr=(struct node*) malloc(sizeof(struct node));
    if(ptr)
    {
        ptr->data=data;
        ptr->pNext=ptr->pPrev=NULL;
    }
    return ptr;
}

int search(int data)
{
    struct node *pCur=pHead;
    int flag=0;
    while(pCur && !flag)
    {
        if(pCur->data==data)
        {
            flag=1;
        }
        else
            pCur=pCur->pNext;
    }
    return flag;
}

//void printall(int m)
void printall(myDisplay getDisplay)
{
    struct node *tmp;
    int n = 1;
    if(pHead == NULL)
    {
        printf(" No data found in the List yet.");
    }
    else
    {
        tmp = pHead;
        //if (m==1)
        if (getDisplay == FirstEntry)
        {
            printf("\n Data entered in the list are :\n");
        }
        else
        {
            printf("\n After deletion the new list are :\n");
        }
        while(tmp != NULL)
        {
            printf(" node %d : %d\n", n, tmp->data);
            n++;
            tmp = tmp->pNext; // current pointer moves to the next node
        }
    }
}

int addnode(int data)
{
    int flag=0;
    struct node *ptr=createnode(data);
    if(ptr)
    {
        flag=1;
        if(pTail==NULL) //Empty List
            pTail=pHead=ptr;
        else // List exists so add at end
        {
            pTail->pNext=ptr;
            ptr->pPrev=pTail;
            pTail=ptr;
        }
    }
    return flag;
}

int insert(int data, int loc)
{
    int flag=0, i=0;
    struct node *ptr, *pCur;
    ptr=createnode(data);
    if(ptr)
    {
        flag=1;
        if(!pHead)  //Empty List
            pHead=pTail=ptr;
        else if(loc==0)
        {
            // as first location
            ptr->pNext=pHead;
            pHead->pPrev=ptr;
            pHead=ptr;
        }
        else
        {
            pCur=pHead;
            for(;(i<loc-1 && pCur);i++)
                pCur=pCur->pNext;
            if(!pCur || pCur==pTail)        //reached Tail
            {
                ptr->pPrev=pTail;
                pTail->pNext=ptr;
                pTail=ptr;
            }
            else
            {
                pCur->pNext->pPrev=ptr;
                ptr->pNext=pCur->pNext;
                pCur->pNext=ptr;
                ptr->pPrev=pCur;
            }
        }
    }
    return flag;
}

int delete(int loc)
{
    /*
        1- There are current struct to delete but first:
            a- it contains pPrev and pNext.
        2- By know the 1.a-pPrev and 1.a-PNext do the below:
            a- Assign 1.a-pPrev->pNext = 1.a-PNext.
            b- Assign 1.a-pNext->pPrev = 1.a-pPrev.
        3- free current struct from memory.
    */
    int flag=0, i=0;
    struct node *curNode;
    curNode = pHead;

    for(i=1; i<loc && curNode!=NULL; i++)
    {
        curNode = curNode->pNext;
    }

    if(loc == 1)
    {
        flag=DelListDeleteFirstNode();
    }
    else if(curNode == pTail)
    {
        flag=DelListDeleteLastNode();
    }
    else if(curNode != NULL)
    {
        curNode->pPrev->pNext = curNode->pNext;
        curNode->pNext->pPrev = curNode->pPrev;

        free(curNode); //Delete the n node
        flag=1;
    }
    else
    {
        printf(" The given position is invalid!\n");
    }
    return flag;
}


int DelListDeleteFirstNode()
{
    int flag=0;
    struct node *NodeToDel;
    if(pHead == NULL)
    {
        printf(" Delete is not possible. No data in the list.\n");
    }
    else
    {
        NodeToDel = pHead;
        if(pHead->pNext != NULL)
        {
            pHead = pHead->pNext;   // move the next address of starting node to 2 node
        }
        pHead->pPrev = NULL;      // set previous address of staring node is NULL
        free(NodeToDel);            // delete the first node from memory
        flag = 1;
    }
    return flag;
}

int DelListDeleteLastNode()
{
    int flag=0;
    struct node *NodeToDel;

    if(pTail == NULL)
    {
        printf(" Delete is not possible. No data in the list.\n");
    }
    else
    {
        NodeToDel = pTail;
        pTail = pTail->pPrev;    // move the previous address of the last node to 2nd last node
        pTail->pNext = NULL;     // set the next address of last node to NULL
        free(NodeToDel);            // delete the last node
        flag = 1;
    }
    return flag;
}
