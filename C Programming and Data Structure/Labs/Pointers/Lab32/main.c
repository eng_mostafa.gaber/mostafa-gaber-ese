/*
 ============================================================================
 Name        : Lab32.c
 Created on  : Mar 8, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Get 2 integer value from the user.
    2- Function to add the user variables and will not return anything but store
    the result in the first variable.
    3- In main function Print the first variable .[updated]
 ============================================================================

 PSEUDOCODE:
 print request for input
        "Enter a number one : "
        "Enter a number two : "
 READ num1, num2
 SWAP(num1,num2)

SWAP:
 num1 = num1 + num2

 WRITE "The summation of two numbers are " + num1
*/

#include <stdio.h>
//#include <stdlib.h>


void Add(int *Pvar1, int *Pvar2);

int main()
{
    int num1=0, num2=0;
    printf("Enter a number one : ");
    scanf("%d", &num1);
    printf("Enter a number two : ");
    scanf("%d", &num2);
    Add(&num1, &num2);
    printf("\nThe summation of two numbers are %d", num1);
    return 0;
}

void Add(int *Pvar1, int *Pvar2)
{
    *Pvar1 += *Pvar2;
}
