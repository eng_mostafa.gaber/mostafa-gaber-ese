/*
 ============================================================================
 Name        : Lab31.c
 Created on  : Mar 8, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Function to swap local variables in main entered by user
    2- Prototype for the function.
    3- Print the swapped variables.
 ============================================================================

 PSEUDOCODE:
 print request for input
        "Enter a number one : "
        "Enter a number two : "
 READ num1, num2
 SWAP(num1,num2)

SWAP:
 num3 = 0
 num3 = num1
 num1 = num2
 num2 = num3
 WRITE "The first swapped value is " + num1 + ", the second value is " + num2
*/


#include <stdio.h>
//#include <stdlib.h>

void Swap(int *Pvar1, int *Pvar2);

int main()
{
    int num1=0, num2=0;
    printf("Enter a number one : ");
    scanf("%d", &num1);
    printf("Enter a number two : ");
    scanf("%d", &num2);
    Swap(&num1, &num2);
    printf("\nThe first swapped value is %d, the second value is %d", num1, num2);
    return 0;
}

void Swap(int *Pvar1, int *Pvar2)
{
    int temp = *Pvar1;
    *Pvar1 = *Pvar2;
    *Pvar2 = temp;
}
