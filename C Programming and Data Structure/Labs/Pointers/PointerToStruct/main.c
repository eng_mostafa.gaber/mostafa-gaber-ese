#include <stdio.h>
#include <string.h>
//#include <stdlib.h>

typedef struct {
    char name[20];
    char age;
    int salary;
} emp;

void task(emp *emp1);

int main()
{
    emp empEntity[3];
    emp *emp_master;
    strcpy(empEntity[0].name, "Mostafa Gaber");
    empEntity[0].age = 28;
    empEntity[0].salary = 3500;
    emp_master = &empEntity[0];
    task(emp_master);

    strcpy(empEntity[1].name, "Tassneem Mostafa");
    empEntity[1].age = 16;
    empEntity[1].salary = 5000;
    emp_master = &empEntity[1];
    task(emp_master);

    strcpy(empEntity[2].name, "Bassant Mostafa");
    empEntity[2].age = 15;
    empEntity[2].salary = 3000;
    emp_master = &empEntity[2];
    task(emp_master);
    return 0;
}

void task(emp *emp1)
{
    printf("%s\n", emp1->name);
    printf("%d\n", emp1->age);
    printf("%d\n", emp1->salary);
}
