#include "calendar.h"
#include <stdio.h>
#include <stdlib.h>
union uniCal myCal;

/*
void SetCalendar(unsigned int day,unsigned int month,unsigned int year)
{
    myCal.day = day;
    myCal.month = month;
    myCal.year = year;
}
*/

void SetDay(unsigned int day)
{
    myCal.day = day;
}
void SetMonth(unsigned int month)
{
    myCal.month = month;
}
void SetYear(unsigned int year)
{
    myCal.year = year;
}
void ShowCalendar()
{
     printf("\nThe date is %04d-%02d-%02d", myCal.year,myCal.month, myCal.day);
}
void GetMonth()
{
    printf("\nCurrent month is %d\n", myCal.month);
}

void GetYear()
{
    printf("\nCurrent year is %d\n", myCal.year);
}

void GetDay()
{
    printf("\nCurrent day is %d\n", myCal.day);
}


