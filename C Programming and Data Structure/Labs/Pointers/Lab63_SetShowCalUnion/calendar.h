#ifndef CALENDAR_H_INCLUDED
#define CALENDAR_H_INCLUDED

union uniCal {
    unsigned int day;
    unsigned int month;
    unsigned int year;
};

//void SetCalendar(unsigned int day,unsigned int month,unsigned int year);
void ShowCalendar();

void SetDay(unsigned int day);
void SetMonth(unsigned int month);
void SetYear(unsigned int year);

void GetDay();
void GetMonth();
void GetYear();

#endif // CALENDAR_H_INCLUDED
