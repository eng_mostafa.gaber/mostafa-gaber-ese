#include <stdio.h>
#include <stdlib.h>
void PrintArray(int arr1[2][2]);
int main()
{
    int arrOne[2][2];
    int arrTwo[2][2];
    int arrThree[2][2];
    int newValue;
    for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            printf("Enter first array cell %d of %d : ", i, y);
            scanf("%d", &arrOne[i][y]);
        }
    }
    for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            printf("Enter second array cell %d of %d : ", i, y);
            scanf("%d", &arrTwo[i][y]);
        }
    }

    for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            arrThree[i][y] = arrOne[i][y] + arrTwo[i][y];
        }
    }

    /*for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            printf("\nThe value %d of %d = %d", i, y, arrThree[i][y]);
        }
    }*/
    PrintArray(arrThree);
    return 0;
}

void PrintArray(int arr1[2][2])
{
    for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            printf("\nThe third cell %d of %d value = %d", i, y, arr1[i][y]);
        }
    }
}

