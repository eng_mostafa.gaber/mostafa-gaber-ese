#ifndef BINARYSEARCH_H_INCLUDED
#define BINARYSEARCH_H_INCLUDED

int BinarySearch();
void FillArray();
void SortArray();

char Compare(int elm1, int elm2);
void SwapVar(int *elm1, int *elm2);

#endif // BINARYSEARCH_H_INCLUDED
