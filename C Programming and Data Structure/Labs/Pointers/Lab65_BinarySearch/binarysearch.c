#include <stdio.h>
#include <stdlib.h>
#include "binarysearch.h"

int searchME[10];

char (*PtrCompare)(int, int);

void FillArray()
{
    for(int i=0; i < 10; i++)
    {
        printf("Enter Index of array %d: ", i + 1);
        scanf("%d", &searchME[i]);
    }
    //SortArray();
    //return 1;
}


void SortArray()
{
    //int val = searchME[0];
    int temp = 0;


    PtrCompare = Compare;


    // Bubble Sort
    for(int i=0; i < 10; i++)
    {
        for(int y= i + 1; y < 10; y++)
        {
            //if(searchME[i] > searchME[y])
            if((*PtrCompare) (searchME[i], searchME[y]))
            {
                /*temp = searchME[i];
                searchME[i] = searchME[y];
                searchME[y] = temp;*/
                SwapVar(&searchME[i], &searchME[y]);
            }
        }
    }
    printf("Sorted Array...\n");

    for(int i=0; i < 10; i++)
    {
        printf("The Index of array %d: %d\n", i + 1, searchME[i]);
    }
    //return 1;
}


char Compare(int elm1, int elm2)
{
    if(elm1 > elm2)
        return 1;
    else
        return 0;
}

void SwapVar(int *elm1, int *elm2)
{
    int temp = 0;
    temp = *elm1;
    *elm1 = *elm2;
    *elm2 = temp;
}
int BinarySearch()
{
    /*int key, boMe = 0, LOW = 0;
    int MED = (10 -1) / 2;
    int HI = (10 -1);
    int arrSize = 0;
    char Right = -1, Left = -1;
    printf("Enter a key to search for: ");
    scanf("%d", & key);
    while(1)
    {
        if(key == searchME[MED])
        {
            //boMe = i;
            break;
        }
        if(key > searchME[MED])
        {
            LOW = MED;
            MED = (HI - MED) / 2;
            if(Right == -1)
                Right = 0;
        }
        else if(key < searchME[MED])
        {
            HI = MED;
            MED = (HI - LOW) / 2;
            LOW = 0;  // need to change it

            if(Left == -1)
                Left = 0;
        }
    }*/

    int c, first, last, middle, n=10, search, boMe = 0;/*, array[100];

    printf("Enter number of elements\n");
    scanf("%d",&n);

    printf("Enter %d integers\n", n);

    for (c = 0; c < n; c++)
    scanf("%d",&array[c]);*/

    printf("Enter value to find\n");
    scanf("%d", &search);

    first = 0;
    last = n - 1;
    middle = (first+last)/2;

    while (first <= last)
    {
        if (searchME[middle] < search)
            first = middle + 1;
        else if (searchME[middle] == search)
        {
            boMe = middle+1;
            printf("%d found at location %d.\n", search, boMe);
            break;
        }
        else
            last = middle - 1;
        middle = (first + last)/2;
    }
    if (first > last)
        printf("Not found! %d isn't present in the list.\n", search);
    return boMe;
}
