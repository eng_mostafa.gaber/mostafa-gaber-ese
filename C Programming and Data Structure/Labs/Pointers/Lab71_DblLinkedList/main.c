#include <stdio.h>
#include <stdlib.h>
#include "DblLinkedList.h"

int main()
{
    int num,data,insPlc,swch, menu;
    int Counter = 1;
	printf("\n\n Doubly Linked List : Delete node from any position of a doubly linked list :\n");
	printf("----------------------------------------------------------------------------------\n");

    printf(" Input the number of nodes (3 or more ): ");
    scanf("%d", &num);
    if(num >= 1)
    {
        for(int i = 1; i <= num; i++)
        {
            printf(" Input data for node %d : ", i);
            scanf("%d", &data);
            addnode(data);
        }
    }
    swch=1;
    printall(swch);
    printf(" Input the position ( 1 to %d ) to delete a node : ", num);
    scanf("%d", &insPlc);

    if(insPlc < 1 || insPlc > num)
    {
        printf("\n Invalid position. Try again.\n ");
    }
    if(insPlc >= 1 && insPlc <= num)
    {
        delete(insPlc);
        swch=2;
        printall(swch);
    }
    return 0;
}
