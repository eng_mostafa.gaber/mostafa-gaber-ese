#ifndef CALENDAR_H_INCLUDED
#define CALENDAR_H_INCLUDED

typedef struct{
    unsigned int day;
    unsigned int month;
    unsigned int year;
} cal;

void SetCalendar(unsigned int day,unsigned int month,unsigned int year);
void ShowCalendar();
void GetDay();
void GetMonth();
void GetYear();

#endif // CALENDAR_H_INCLUDED
