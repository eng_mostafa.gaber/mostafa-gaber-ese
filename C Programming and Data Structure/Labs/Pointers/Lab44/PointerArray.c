#include "pointerarray.h"


void PrintCharArry(char *CA, int ASize)
{
    for(int index = 0; index < ASize; index++)
    {
        printf("The element value is %c, and the element address is %X\n", *CA++, CA);
    }
}

void PrintIntArry(int *IA, int ASize)
{
    for(int index = 0; index < ASize; index++)
    {
        printf("The element value is %d, and the element address is %X\n", *IA++, IA);
    }
}

