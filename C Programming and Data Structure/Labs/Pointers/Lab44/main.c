#include <stdio.h>
#include "PointerArray.h"
//#include <stdlib.h>

int main()
{
    char char_arr[5]="Allah";
    int int_arr[5]={1,2,3,4,5};

    void (*PtrPrintCharArry)(char*, int);
    void (*PtrPrintIntArry) (int*, int);

    PtrPrintCharArry = PrintCharArry;
    PtrPrintIntArry = PrintIntArry;

    (*PtrPrintCharArry)(char_arr, 5);
    (*PtrPrintIntArry) (int_arr, 5);

    //printf("Hello world!\n");
    return 0;
}
