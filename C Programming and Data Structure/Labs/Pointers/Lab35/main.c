/*
 ============================================================================
 Name        : Lab34.c
 Created on  : Mar 8, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Create a varaible named maximum with value 1000.
    2- Enquarage user to enter new value.
    3- Check if the new value is greater than saved value or not.
    4- If new value is greater than saved value then update variable maximum with new value.
    using #define function


 ============================================================================

 PSEUDOCODE:
    1- Create a varaible named maximum with value 1000.
    2- Enquarage user to enter new value.
    3- Check if the new value is greater than saved value or not.
    4- If new value is greater than saved value then update variable maximum with new value.
*/

#include <stdio.h>
#include "main.h"
//#include <stdlib.h>

int main()
{
    int FirstNum;
    int SecondNum;
    int ResultsMax, ResultsMin;

    printf("Enter First value ");
    scanf("%d", &FirstNum);
    printf("Enter Second value ");
    scanf("%d", &SecondNum);

    ResultsMax = GetMaxValue(FirstNum, SecondNum);
    ResultsMin = GetMinValue(FirstNum, SecondNum);

    printf("\nThe maximum value is %d", ResultsMax);
    printf("\nThe minimum value is %d", ResultsMin);
    return 0;
}
