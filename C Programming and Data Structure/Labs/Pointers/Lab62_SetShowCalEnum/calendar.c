#include "calendar.h"
#include <stdio.h>
#include <stdlib.h>
//day myDay;
typedef enum month myMonth;
myMonth currMonth;

typedef enum weekday myWDay;
myWDay currWDay;
void SetCalendar(unsigned int day,unsigned int month,unsigned int year)
{
    myDay = day;
    currMonth = month;
    myYear = year;
}

void ShowCalendar()
{
     printf("\nThe date is %04d-%02d-%02d", myYear,currMonth, myDay);
}

void GetDay()
{
    printf("\nCurrent day is %d", myDay);
}

void GetMonth()
{
    printf("\nCurrent month is %d", currMonth);
}

void GetYear()
{
    printf("\nCurrent year is %d", myYear);
}

void SetWeekDay(char wDay)
{
    currWDay = wDay;
}
void GetWeekDay()
{
    switch(currWDay)
    {
    case 1:
        printf("\nCurrent week day is Saturday");
        break;
    case 2:
        printf("\nCurrent week day is Sunday");
        break;
    case 3:
        printf("\nCurrent week day is Monday");
        break;
    case 4:
        printf("\nCurrent week day is Tuesday");
        break;
    case 5:
        printf("\nCurrent week day is Wednesday");
        break;
    case 6:
        printf("\nCurrent week day is Thursday");
        break;
    case 7:
        printf("\nCurrent week day is Friday");
        break;
    }
}

