#include <stdio.h>
#include <stdlib.h>

#ifndef CALENDAR_H_INCLUDED
#define CALENDAR_H_INCLUDED

//enum day {jan=1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
//enum month {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
enum weekday {Saturday=1, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday};

unsigned int myDay;
enum month {January=1, February, March, April, May, June, July, August, September, October, November, December};
unsigned int myYear;

void SetCalendar(unsigned int day,unsigned int month,unsigned int year);
void ShowCalendar();
void GetDay();
void GetMonth();
void GetYear();

void SetWeekDay(char wDay);
void GetWeekDay();

#endif // CALENDAR_H_INCLUDED
