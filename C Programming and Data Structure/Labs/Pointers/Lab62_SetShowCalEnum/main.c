#include <stdio.h>
#include "calendar.h"
//#include <stdlib.h>

int main()
{
    unsigned int day;
    unsigned int month;
    unsigned int year;
    char wDay;
    while(1)
    {
        printf("Enter the week day between 1 & 7: ");
        scanf("%d", &wDay);
        if(wDay <= 7 && wDay > 0)
        {
            SetWeekDay(wDay);
            break;
        }
    }
    while(1)
    {
        printf("Enter the day between 1 & 31: ");
        scanf("%d", &day);
        if(day <= 31 && day > 0)
            break;
    }
    while(1)
    {
        printf("Enter the month between 1 & 12: ");
        scanf("%d", &month);
        if(month <= 12 && month > 0)
            break;
    }
    while(1)
    {
        printf("Enter the year between 1 & 9999: ");
        scanf("%d", &year);
        if(year <= 9999 && year > 0)
            break;
    }
    SetCalendar(day, month, year);
    ShowCalendar();
    GetDay();
    GetMonth();
    GetYear();
    GetWeekDay();
    return 0;
}
