#ifndef CALCULATOR_H_INCLUDED
#define CALCULATOR_H_INCLUDED

#define RUN 1
//#define OUT 1

    #ifdef RUN
        int Add(int num1, int num2);
        int Subtract(int num1, int num2);
        int Multiplication(int num1, int num2);
        int Division(int num1, int num2);
        int Module(int num1, int num2);
    #elif OUT
        void OutOfDebug();
    #endif // RUN

#endif // CALCULATOR_H_INCLUDED
