#include "calculator.h"


#ifdef RUN
    int Add(int num1, int num2)
    {
        return num1 + num2;
    }

    int Subtract(int num1, int num2)
    {
        return num1 - num2;
    }

    int Multiplication(int num1, int num2)
    {
        return num1 * num2;
    }

    int Division(int num1, int num2)
    {
        return num1 / num2;
    }

    int Module(int num1, int num2)
    {
        return num1 % num2;
    }

#elif OUT
    void OutOfDebug()
    {
        printf("You are out of DEBUG mode!!!\n");
    }
#endif // RUN
