#include <stdio.h>
#include <stdlib.h>
void PrintArray(int arr1[2][2]);
int main()
{
    int arr[2][2];
    int newValue;
    for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            printf("Enter value %d of %d : ", i, y);
            /*scanf("%d", &newValue);
            arr[i][y]= newValue;
            */
            scanf("%d", &arr[i][y]);
        }
    }

    for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            printf("\nThe value %d of %d = %d", i, y, arr[i][y]);
        }
    }
    PrintArray(arr);
    return 0;
}

void PrintArray(int arr1[2][2])
{
    for(int i=0; i<2; i++)
    {
        for(int y=0; y<2; y++)
        {
            printf("\nThe value %d of %d = %d", i, y, arr1[i][y]);
        }
    }
}

