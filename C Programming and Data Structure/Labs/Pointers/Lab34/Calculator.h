#ifndef CALCULATOR_H_INCLUDED
#define CALCULATOR_H_INCLUDED

int Add(int num1, int num2);
int Subtract(int num1, int num2);
int Multiplication(int num1, int num2);
int Division(int num1, int num2);
int Module(int num1, int num2);

#endif // CALCULATOR_H_INCLUDED

