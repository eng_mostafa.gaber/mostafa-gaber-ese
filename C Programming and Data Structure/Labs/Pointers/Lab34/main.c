/*
 ============================================================================
 Name        : Lab34.c
 Created on  : Mar 8, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Add two files, Calculator.c and Calculator.h.
    2- Define all prototype in Calculator.h.
    3- Implement all Calculator function in Calculator.c.
    4- Call all Calculator function in main.c.
    5- print the result.


    1- Print First variable value.
    2- Print Second variable value.
    3- Enter First value.
    4- Enter Second value.
    5- Enter the operator.
    6- Print the calculation formula.
    7- Print the result.
 ============================================================================

 PSEUDOCODE:
 Module Calculator

    Add(num1, num2):
        Report num1 + num2

    Subtract(num1, num2):
        Report num1 - num2

    Multiplication(num1, num2):
        Report num1 * num2

    Division(num1, num2):
        Report num1 / num2

    Module(num1, num2):
        Report num1 % num2
 END Module

 Module Main
    PRINT request for input
        "Enter First value "
        "Enter Second value "
    READ FirstNum, SecondNum
    PRINT request for input
        "Enter the operator (+, -, * or /) : "
    READ OperatorStr

    If the OperatorStr = '+' Then
        Result = Calculator.Add(FirstNum, SecondNum)
    Elseif the OperatorStr = '-' Then
        Result = Calculator.Subtract(FirstNum, SecondNum)
    Elseif the OperatorStr = '*' Then
        Result = Calculator.Multiplication(FirstNum, SecondNum)
    Elseif the OperatorStr = '/' Then
        IF SecondNum = 0
            WRITE "Divistion by zero, Enter second No. gerater than zero")
            EXIT IF
        Result = Calculator.Division(FirstNum, SecondNum)
    Endif
    WRITE FirstNum + " " + OperatorStr + " " + SecondNum + " " + Result
 END Module
*/

#include <stdio.h>
#include "Calculator.h"
//#include <stdlib.h>


int FirstNum;
int SecondNum;
char OperatorStr;
int Result;

int main()
{
    printf("Enter First value ");
    scanf("%d", &FirstNum);
    printf("Enter Second value ");
    scanf("%d", &SecondNum);

    //scanf("%c", &OperatorStr);

    printf("\nEnter the operator (+, -, * or /) : ");
    scanf("%s", &OperatorStr);
    switch(OperatorStr){
    case '+':
        Result = Add(FirstNum, SecondNum);
        break;
    case '-':
        Result = Subtract(FirstNum, SecondNum);
        break;
    case '*':
        Result = Multiplication(FirstNum, SecondNum);
        break;
    case '/':
        if(SecondNum == 0)
        {
            printf("\nDivistion by zero, Enter second No. gerater than zero");
            return 0;
        }
        Result = Division(FirstNum, SecondNum);
        break;
    }
    printf("\n%d %c %d = %d",FirstNum, OperatorStr, SecondNum, Result);
    return 0;
}
