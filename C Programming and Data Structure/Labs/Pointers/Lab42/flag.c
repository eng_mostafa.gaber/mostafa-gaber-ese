#include "flag.h"


unsigned char SetFlag(unsigned char x)
{
    x = (1 << 0);
    return x;
}

unsigned char ClearFlag(unsigned char x)
{
    x &= ~ (1 << 0);
    return x;
}

void PrintFlag(unsigned char x)
{
    printf("\nThe flag value is : %d", x);
}
