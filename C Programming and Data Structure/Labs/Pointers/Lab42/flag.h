#ifndef FLAG_H_INCLUDED
#define FLAG_H_INCLUDED


unsigned char SetFlag(unsigned char x);
unsigned char ClearFlag(unsigned char x);
void PrintFlag(unsigned char x);


#endif // FLAG_H_INCLUDED
