#include <stdio.h>
#include "flag.h"
//#include <stdlib.h>

int main()
{
    unsigned char FLAG = 0;


    // D
    unsigned char (*PtrSetFlag) (unsigned char);
    unsigned char (*PtrClearFlag) (unsigned char x);
    void (*PtrPrintFlag) (unsigned char x);

    // Assign function to pointer
    PtrSetFlag = SetFlag;
    PtrClearFlag = ClearFlag;
    PtrPrintFlag = PrintFlag;

    // Call pointer
    FLAG = (*PtrSetFlag) (FLAG);
    (*PtrPrintFlag) (FLAG);
    FLAG = (*PtrClearFlag) (FLAG);
    (*PtrPrintFlag) (FLAG);
    return 0;
}
