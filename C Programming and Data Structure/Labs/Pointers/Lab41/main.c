/*
 ============================================================================
 Name        : Lab34.c
 Created on  : Mar 9, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Declare a flag.
    2- Declare a function to set the flag by 1.
    3- Declare a function to clear the flag by 0
    4- Declare a function to print the flag.


 ============================================================================

 PSEUDOCODE:
    1-
*/

#include <stdio.h>
//#include <stdlib.h>

unsigned char SetFlag(unsigned char x);
unsigned char ClearFlag(unsigned char x);
void PrintFlag(unsigned char x);

int main()
{
    unsigned char FLAG = 0;
    FLAG = SetFlag(FLAG);
    PrintFlag(FLAG);
    FLAG = ClearFlag(FLAG);
    PrintFlag(FLAG);
    return 0;
}


unsigned char SetFlag(unsigned char x)
{
    x = (1 << 0);
    return x;
}

unsigned char ClearFlag(unsigned char x)
{
    x &= ~ (1 << 0);
    return x;
}

void PrintFlag(unsigned char x)
{
    printf("\nThe flag value is : %d", x);
}
