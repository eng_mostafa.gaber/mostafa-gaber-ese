/*
 ============================================================================
 Name        : Lab33.c
 Created on  : Mar 8, 2019
 Author      : Mostafa Abuelnour Gaber
 Version     : 1
 Copyright   : AMIT Learning
 Description : This program is part of C Course On AMIT Learning
 ========================================================
 Implement a program that :
    1- Create loop.
    2- Input from user.
    3- Call function to take input user variable.
    4- Return max from last value and input user variable.
    5- print the max value.
 ============================================================================

 PSEUDOCODE:
 var1 = 1
 exitvar = ""
 REPEAT
    print request for input
        "Enter the current temperature : "
    READ tempr
    TEMPERATURE(tempr)

    TEMPERATURE:
        GLOBAL currtemp = 0
        IF tempr > currtemp
            currtemp = tempr
        ENDIF
        WRITE "The maximum temperature is " + currtemp
UNTIL var1 = 1
*/

#include <stdio.h>
//#include <stdlib.h>

int Temperature(int tempr);

int main()
{
    int temp=0, results;
    while(1)
    {
        printf("\nEnter the current temperature : ");
        scanf("%d", &temp);
        results = Temperature(temp);
        printf("\nThe maximum temperature is %d", results);
    }
    return 0;
}

int Temperature(int tempr)
{
    static int currtemp = 0;
    if(tempr > currtemp)
        currtemp = tempr;
    return currtemp;
}
