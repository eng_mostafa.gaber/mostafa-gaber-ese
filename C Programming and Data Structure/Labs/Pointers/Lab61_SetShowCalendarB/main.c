typedef struct{
    unsigned int day;
    unsigned int month;
    unsigned int year;
} cal;

void SetCalendar(unsigned int day,unsigned int month,unsigned int year);
void ShowCalendar();
void GetDay();
void GetMonth();
void GetYear();

cal myCal;
//cal getCal;
cal *curr_call;

#include <stdio.h>

//#include <stdlib.h>
//extern cal myCal;
//extern cal *curr_call;
//extern cal getCal;
int main()
{
    unsigned int day;
    unsigned int month;
    unsigned int year;
    printf("Enter the day: ");
    scanf("%d", &day);
    printf("Enter the month: ");
    scanf("%d", &month);
    printf("Enter the year: ");
    scanf("%d", &year);
    SetCalendar(day, month, year);
    ShowCalendar();
    GetDay();
    GetMonth();
    GetYear();
    return 0;
}

void SetCalendar(unsigned int day,unsigned int month,unsigned int year)
{
    myCal.day = day;
    myCal.month = month;
    myCal.year = year;
    curr_call = &myCal;
}

void ShowCalendar()
{
    printf("\nThe date is %04d-%02d-%02d", curr_call->year, curr_call->month, curr_call->day);
}

void GetDay()
{
    //printf("Current day is %d", curr_call->day);
    //getCal.day = curr_call->day;
    printf("\nCurrent day is %d", myCal.day);
}

void GetMonth()
{
    printf("\nCurrent month is %d", curr_call->month);
    //getCal.month = curr_call->month;
}

void GetYear()
{
    //getCal.year = curr_call->year;
    printf("\nCurrent year is %d", curr_call->year);
}
