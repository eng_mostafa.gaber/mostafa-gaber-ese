#include <stdio.h>
#include <stdlib.h>
#include "SimpleDb.h"

int main()
{
    int num, data, menu;
    /*
    uint8 Student_ID = 0;
    uint8 Student_Year = 0;
    uint8 subjects[3] = {0},  grades[3] = {0};
    */
    char pressKey=0;
    while(1)
    {
        //printf("\nTrace Error 4 ...");
        clrscr();
        printf("\n\n Doubly Linked List : Delete node from any position of a doubly linked list :\n");
        printf("----------------------------------------------------------------------------------\n");
        printf("\n Enter 1- Add new, 2- Display All IDs of Students..., 3- Search for..., 4- Delete Node..., 5- List All, 6- Exit\n");
        scanf("%d", &menu);
        switch(menu)
        {
        case 1:
            // 1- Add new
            NewRecord();
            break;
        case 2:
            // 2- Display All IDs of Students...
            DisplayAllIDsofStudents();
            break;
        case 3:
            // 3- Search for...
            SearchFor();
            break;
        case 4:
            // 4- Delete Node...
            DeleteRecord();
            break;
        case 5:
            // 5- List All
            printall(All);
            //printf("\nTrace Error 2 ...");
            break;
        case 6:
            // 6- Exit
            DestroyDatabase();
            return 0;
            break;
        }
        printf("\nTrace Error 3 ...");
    }
    return 0;
}
