#ifndef SIMPLEDB_H_INCLUDED
#define SIMPLEDB_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#define MaxRec  10
#define Debug 0
enum Display {FirstEntry=1, AfterDelete, All};
typedef enum { false, true } bool;
typedef enum Display myDisplay;
//myDisplay currDisplay;
typedef unsigned int uint8;


struct node
{
    uint8 Student_ID;
    uint8 Student_Year;
    uint8 Course_1_ID;
    uint8 Course_1_Grade;
    uint8 Course_2_ID;
    uint8 Course_2_Grade;
    uint8 Course_3_ID;
    uint8 Course_3_Grade;
    struct node *pNext;
    struct node *pPrev;
};

struct IDs
{
    uint8 Student_ID;
    struct IDs *pNext;
    struct IDs *pPrev;
};

void clrscr();

struct node* createnode(uint8 id, uint8 year, uint8 (*subjects)[3], uint8 (*grades)[3]);
bool SDB_IsFull();
uint8 SDB_GetUsedSize();
uint8 GetSubject(uint8 subj);
uint8 GetGrade(uint8 num);

void SDB_GetIdList(uint8* count, uint8 (*list)[10]);
void DisplayAllIDsofStudents();

bool SDB_IsIdExist(uint8 ID);
//void printall(int m);
void printall(myDisplay getDisplay);
bool SDB_AddEntry(uint8 id, uint8 year, uint8 (*subjects)[3], uint8 (*grades)[3]);
//int insert(int data, int loc);
void SDB_DeleteEntry(uint8 id);
bool SDB_ReadEntry(uint8 id, uint8* year, uint8 (*subjects)[3], uint8 (*grades)[3]);
void SearchFor();
void NewRecord();
void DeleteRecord();
void DestroyDatabase();
int DelListDeleteFirstNode();
int DelListDeleteLastNode();

#endif // SIMPLEDB_H_INCLUDED
