#include "SimpleDb.h"

struct node *pHead;
struct node *pTail;

struct IDs *pIDHead;
struct IDs *pIDTail;
/*
typedef enum Display myDisplay;

myDisplay currDisplay;*/


void clrscr()
{
    #if(Debug)
        printf("\nTrace Error 5 ...");
    #endif
    system("@cls||clear");
    #if(Debug)
        printf("\nTrace Error 6 ...");
    #endif
}

uint8 GetSubject(uint8 subj)
{
    uint8 subject = 0;
    printf("Enter Subject No. %d: ", subj + 1);
    scanf("%d", &subject);
    #if(Debug)
        printf("The Subject No. %d: ", subject);
    #endif
    return subject;
}

uint8 GetGrade(uint8 num)
{
    uint8 grade = 0;
    while(1)
    {
        printf("Enter Grade No. %d between 0 and 100: ", num + 1);
        scanf("%d", &grade);
        if(grade <= 100 && grade > 0)
        {
            break;
        }
    }
    #if(Debug)
        printf("The Grade No. %d: \n", grade);
    #endif
    return grade;
}

struct node* createnode(uint8 id, uint8 year, uint8 (*subjects)[3], uint8 (*grades)[3])
{
    struct node *ptr=NULL;
    ptr = (struct node*) malloc(sizeof(struct node));
    if(ptr)
    {
        #if(Debug)
            printf(" \n4- Input Student ID is : %d", id);
            scanf("%d", &id);
        #endif
        ptr->Student_ID     = id;
        ptr->Student_Year   = year;
        ptr->Course_1_ID    = (*subjects)[0]; //GetSubject(1);
        #if(Debug)
            printf(" \nptr->Course_1_ID is : %d, = (*subjects)[0] = %d\n", ptr->Course_1_ID, (*subjects)[0]);
            printf(" \nPress any key to continue...");
            scanf("%d", &id);
            ptr->Course_1_Grade = (*grades)[0]; //GetGrade(1);
            ptr->Course_2_ID    = (*subjects)[1]; //GetSubject(2);
            ptr->Course_2_Grade = (*grades)[1]; //GetGrade(2);
            ptr->Course_3_ID    = (*subjects)[2]; //GetSubject(3);
            ptr->Course_3_Grade = (*grades)[2]; //GetGrade(3);
        #endif
        ptr->Course_1_Grade = (*grades)[0];
        ptr->Course_2_ID    = (*subjects)[1];
        ptr->Course_2_Grade = (*grades)[1];
        ptr->Course_3_ID    = (*subjects)[2];
        ptr->Course_3_Grade = (*grades)[2];
        ptr->pNext=ptr->pPrev= NULL;
    }
    return ptr;
}

void NewRecord()
{
    uint8 Student_ID = 0;
    uint8 Student_Year = 0;
    uint8 subjects[3] = {0},  grades[3] = {0};
    uint8 temp;
    char pressKey=0;
    printf(" Input Student ID : ");
    scanf("%d", &Student_ID);
    #if(Debug)
        printf(" \n1- Input Student ID is : %d", Student_ID);
    #endif
    if(SDB_IsIdExist(Student_ID))
    {
        printf("The Student ID %d already exists, kindly use another ID", Student_ID);
        scanf("%s", pressKey);
    }
    else
    {
        #if(Debug)
            printf(" \n2- Input Student ID is : %d", Student_ID);
        #endif
        printf(" Input Student Year : ");
        scanf("%d", &Student_Year);
        for(int i=0; i< 3; i++)
        {
            subjects[i]  = GetSubject(i);
           // subjects[i] = temp; //GetSubject(i);
            #if(Debug)
                printf(" \n6- Subject No. %d is : %d", i, subjects[i - 1]);
            #endif
            //temp = GetGrade(i);
            grades[i] = GetGrade(i); //GetGrade(i);
            #if(Debug)
                printf(" \n7- Grades No. %d is : %d", i, grades[i - 1]);
            #endif
        }
        //int SDB_AddEntry(uint8 id, uint8 year, uint8* subjects, uint8* grades)
        SDB_AddEntry(Student_ID, Student_Year, &subjects, &grades);
    }
}

void SDB_GetIdList(uint8* count, uint8 (*list)[10])
{
    struct node *tmp;
    uint8 n = 0;
    if(pHead != NULL)
    {
        tmp = pHead;
        while(tmp != NULL)
        {
            (*list)[n] = tmp->Student_ID;
            tmp = tmp->pNext; // current pointer moves to the next node
            n++;
        }
        *count = n;
    }
    tmp = NULL;
}

void DisplayAllIDsofStudents()
{
    uint8 StudentIDs[10] = {0};
    uint8 IDsCount= 0;
    char pressKey;
    SDB_GetIdList(&IDsCount, &StudentIDs);

    for(int i=0; i< IDsCount; i++)
    {
        printf("Student ID No. %d is : %d\n", i + 1, StudentIDs[i]);
    }
    printf("\nPress any key followed by Enter key to continue...");
    scanf("%s", &pressKey);
}

bool SDB_IsFull()
{
    bool isFull = false;
    if(SDB_GetUsedSize() >= MaxRec)
        isFull = true;
    return isFull;
}

uint8 SDB_GetUsedSize()
{
    struct node *tmp;
    uint8 n = 0;
    if(pHead != NULL)
    {
        tmp = pHead;
        while(tmp != NULL)
        {
            n++;
            tmp = tmp->pNext; // current pointer moves to the next node
        }
    }
    tmp = NULL;
    return n;
}
bool SDB_IsIdExist(uint8 ID)
{
    struct node *pCur=pHead;
    bool flag=false;
    while(pCur && !flag)
    {
        if(pCur->Student_ID==ID)
        {
            flag=true;
        }
        else
            pCur=pCur->pNext;
    }
    pCur = NULL;
    return flag;
}

void printall(myDisplay getDisplay)
{
    struct node *tmp;
    int n = 1;
    char pressKey;
    if(pHead == NULL)
    {
        printf(" No data found in the List yet.");
    }
    else
    {
        tmp = pHead;
        if (getDisplay == FirstEntry)
        {
            printf("\n Data entered in the list are :\n");
        }
        else if(getDisplay == AfterDelete)
        {
            printf("\n After deletion the new list are :\n");
        }
        while(tmp != NULL)
        {
            printf(" node %d Student ID: %d\n", n, tmp->Student_ID);
            printf(" \tStudent Year: %d\n", tmp->Student_Year);
            printf(" \tCourse1 ID: %d\n", tmp->Course_1_ID);
            printf(" \tCourse1 Grade: %d\n", tmp->Course_1_Grade);
            printf(" \tCourse2 ID: %d\n", tmp->Course_2_ID);
            printf(" \tCourse2 Grade: %d\n", tmp->Course_2_Grade);
            printf(" \tCourse3 ID: %d\n", tmp->Course_3_ID);
            printf(" \tCourse3 Grade: %d\n", tmp->Course_3_Grade);
            n++;
            tmp = tmp->pNext; // current pointer moves to the next node
        }
        tmp = NULL;
        if(n > 1)
        {
            printf("\nPress any key followed by Enter key to continue...");
            scanf("%s", &pressKey);

            #if(Debug)
                printf("\nTrace Error 1 ...");
            #endif
        }
    }
}

bool SDB_AddEntry(uint8 id, uint8 year, uint8 (*subjects)[3], uint8 (*grades)[3])
{
    bool flag=false;
    #if(Debug)
        printf(" \n3- Input Student ID is : %d", id);
    #endif
    struct node *ptr = createnode(id, year, subjects, grades);
    //struct node *ptr;
    //createnode(id, year, &subjects, &grades, struct node *ptr);
    if(ptr)
    {
        flag = true;
        if(pTail==NULL) //Empty List
            pTail=pHead=ptr;
        else // List exists so add at end
        {
            pTail->pNext=ptr;
            ptr->pPrev=pTail;
            pTail=ptr;
        }
    }
    ptr = NULL;
    return flag;
}

void SearchFor()
{
    uint8 subjects[3]={0},  grades[3]={0};
    uint8 id, year=0, pressKey;
    printf(" Input Student ID to search for : ");
    scanf("%d", &id);
    if(SDB_ReadEntry(id, &year, &subjects, &grades))
    {
        printf(" \tStudent Year: %d\n", year);

        for(int i=0; i< 3; i++)
        {
            printf(" \tCourse%d ID: %d\n", i + 1, subjects[i]);
            printf(" \tCourse%d Grade: %d\n", i + 1, grades[i]);
        }
        printf("\nPress any key followed by Enter key to continue...");
    }
    else
    {
        printf("\nStudent not found, \nPress any key followed by Enter key to continue...");
    }
    scanf("%s", &pressKey);
}
bool SDB_ReadEntry(uint8 id, uint8* year, uint8 (*subjects)[3], uint8 (*grades)[3])
{
    bool isFound = false;
    struct node *pCur=pHead;
    while(pCur)
    {
        if(pCur->Student_ID == id)
        {
            *year = pCur->Student_Year;
            /* *subjects = pCur->Course_1_ID;
            *grades = pCur->Course_1_Grade;
            */
            (*subjects)[0] = pCur->Course_1_ID;
            (*grades)[0] = pCur->Course_1_Grade;
            (*subjects)[1] = pCur->Course_2_ID;
            (*grades)[1] = pCur->Course_2_Grade;
            (*subjects)[2] = pCur->Course_3_ID;
            (*grades)[2] = pCur->Course_3_Grade;
            pCur = NULL;
            return true;
        }
        else
            pCur=pCur->pNext;
    }
    pCur = NULL;
    return isFound;
}
/*
int insert(int data, int loc)
{
    int flag=0, i=0;
    struct node *ptr, *pCur;
    ptr=createnode(data);
    if(ptr)
    {
        flag=1;
        if(!pHead)  //Empty List
            pHead=pTail=ptr;
        else if(loc==0)
        {
            // as first location
            ptr->pNext=pHead;
            pHead->pPrev=ptr;
            pHead=ptr;
        }
        else
        {
            pCur=pHead;
            for(;(i<loc-1 && pCur);i++)
                pCur=pCur->pNext;
            if(!pCur || pCur==pTail)        //reached Tail
            {
                ptr->pPrev=pTail;
                pTail->pNext=ptr;
                pTail=ptr;
            }
            else
            {
                pCur->pNext->pPrev=ptr;
                ptr->pNext=pCur->pNext;
                pCur->pNext=ptr;
                ptr->pPrev=pCur;
            }
        }
    }
    return flag;
}
*/
void SDB_DeleteEntry(uint8 loc)
{
    int flag=0, i=0;
    struct node *curNode;
    curNode = pHead;

    for(i=1; i<loc && curNode!=NULL; i++)
    {
        curNode = curNode->pNext;
    }

    if(loc == 1)
    {
        flag=DelListDeleteFirstNode();
    }
    else if(curNode == pTail)
    {
        flag=DelListDeleteLastNode();
    }
    else if(curNode != NULL)
    {
        curNode->pPrev->pNext = curNode->pNext;
        curNode->pNext->pPrev = curNode->pPrev;

        free(curNode); //Delete the n node
        //flag=1;
    }
    else
    {
        printf(" The given position is invalid!\n");
    }
    //return flag;
    curNode = NULL;
}

void DeleteRecord()
{
    uint8 insPlc, Counter = 0;
    //printall(FirstEntry);
    Counter = SDB_GetUsedSize();
    printf(" Input the position ( 1 to %d ) to delete a node : ", Counter);
    scanf("%d", &insPlc);

    if(insPlc < 1 || insPlc > Counter)
    {
        printf("\n Invalid position. Try again.\n ");
    }
    if(insPlc >= 1 && insPlc <= Counter)
    {
        SDB_DeleteEntry(insPlc);
        printall(AfterDelete);
        Counter--;
    }
}

void DestroyDatabase()
{
    uint8 Counter = 0;
    Counter = SDB_GetUsedSize();
    for(int i=Counter; i > 0; i--)
        SDB_DeleteEntry(i);
}

int DelListDeleteFirstNode()
{
    int flag=0;
    struct node *NodeToDel;
    if(pHead == NULL)
    {
        printf(" Delete is not possible. No data in the list.\n");
    }
    else
    {
        NodeToDel = pHead;
        if(pHead->pNext != NULL)
        {
            pHead = pHead->pNext;   // move the next address of starting node to 2 node
        }
        pHead->pPrev = NULL;      // set previous address of staring node is NULL
        free(NodeToDel);            // delete the first node from memory
        flag = 1;
    }
    return flag;
}

int DelListDeleteLastNode()
{
    int flag=0;
    struct node *NodeToDel;

    if(pTail == NULL)
    {
        printf(" Delete is not possible. No data in the list.\n");
    }
    else
    {
        NodeToDel = pTail;
        pTail = pTail->pPrev;    // move the previous address of the last node to 2nd last node
        pTail->pNext = NULL;     // set the next address of last node to NULL
        free(NodeToDel);            // delete the last node
        flag = 1;
    }
    return flag;
}
