#include "stm32f10x.h"

#include "lcd.h"
 
int main(void)
{
    InitializeLCD(); //Initialize LCD
    ClearLCDScreen(); //CLEAR LCD
    Cursor(0,2); //Set Cursor to the First line second Character
    PrintStr("Embedded Group"); //Print String 
    Cursor(1,9);  //second line
    PrintStr("O7");
   // delay(32000);
 
    while(1)
    {
			//ClearLCDScreen(); //CLEAR LCD
			Cursor(0,2); //Set Cursor to the First line second Character
			PrintStr("Embedded Group"); //Print String 
			delay(1000);
			if(GPIOA->IDR & 0x0001)
			{
				Cursor(1,1);  //second line
				PrintStr("Button On ");
				delay(1500);
			}
			else
			{
				Cursor(1,1);  //second line
				PrintStr("Button Off");
				delay(1500);
			}
    }
}