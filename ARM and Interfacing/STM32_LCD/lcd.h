#include <stm32f10x.h>
#define GPIOB_CLOCKEN      (1<<3)
#define GPIOA_CLOCKEN      (1<<2)

// GPIO->ODR
#define GPIO_Pin_0              0x0001	// 0000 0000 0000 0001
#define GPIO_Pin_1              0x0002	// 0000 0000 0000 0010
#define GPIO_Pin_2              0x0004	// 0000 0000 0000 0100
#define GPIO_Pin_3              0x0008	// 0000 0000 0000 1000
#define GPIO_Pin_4              0x0010	// 0000 0000 0001 0000
#define GPIO_Pin_5              0x0020	// 0000 0000 0010 0000
#define GPIO_Pin_6              0x0040	// 0000 0000 0100 0000
#define GPIO_Pin_7              0x0080	// 0000 0000 1000 0000

//#define GPIO_Pin_15             0x8000	// 1000 0000 1000 0000

//GPIO->CRL
#define GPIO_OUTPUT							0x33333333
#define GPIO_Pin_0_OUTPUT 			(3<<0)
#define GPIO_Pin_1_OUTPUT 			(3<<4)
#define GPIO_Pin_2_OUTPUT 			(3<<8)
#define GPIO_Pin_3_OUTPUT 			(3<<12)
#define GPIO_Pin_4_OUTPUT 			(3<<16)
#define GPIO_Pin_5_OUTPUT 			(3<<20)
#define GPIO_Pin_6_OUTPUT 			(3<<24)
#define GPIO_Pin_7_OUTPUT 			(3<<28)


// Pin definition
//#define LCD16X2_PIN_RW             GPIO_Pin_13

#define     LCM_OUT               GPIOB->ODR
#define     LCM_PIN_RS            GPIO_Pin_0          // PB0
#define     LCM_PIN_EN            GPIO_Pin_1          // PB1
#define     LCM_PIN_D7            GPIO_Pin_7          // PB7
#define     LCM_PIN_D6            GPIO_Pin_6          // PB6
#define     LCM_PIN_D5            GPIO_Pin_5          // PB5
#define     LCM_PIN_D4            GPIO_Pin_4          // PB4
#define     LCM_PIN_MASK  ((LCM_PIN_RS | LCM_PIN_EN | LCM_PIN_D7 | LCM_PIN_D6 | LCM_PIN_D5 | LCM_PIN_D4))

#define			BTN_PIN_MASK	       (GPIO_Pin_0)
#define			GPIOA_PA0						(GPIOA->IDR & 0x0001)

void delay(int a);



void PulseLCD();

void SendByte(char ByteToSend, int IsData);


void Cursor(char Row, char Col);


void ClearLCDScreen();



void InitializeLCD(void);

void PrintStr(char *Text);
