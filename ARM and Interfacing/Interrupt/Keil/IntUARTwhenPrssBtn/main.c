#include <stm32f10x.h>


int main()
{
	int x=0,j = 0;
	NVIC->ISER[0] = (1<<6); // Enable USART1 Int. 
				// NVIC == page 198  EXTI Line0 interrupt
				// link with page 119 in programming manual
	RCC->APB2ENR |= (1<<14) | (1<<0) | (1<<2) | (1<<3) | (1<<4); // and GPIOA, B, C
	//NVIC->ISER[1] = (1<<5); // Enable USART1 Int. from NVIC
	
	//NVIC->ICER[1] = (1<<5); // Clear-enable
	//NVIC->ISPR[1] = (1<<5); // Set-pending
	//NVIC->ICPR[1] = (1<<5); // Clear-pending
	//NVIC->IABR[1] = (1<<5); // Active Bit

	AFIO->EXTICR[0] = 0x0000;
	
	GPIOA->CRL = 0x00000004; // Pin 0 enable & Pin 5 
	GPIOB->CRH = 0x00300000;  // Led on Pin 13
	EXTI->RTSR = (1 << 0);
	EXTI->FTSR = (1 << 0);
	EXTI->IMR = (1 << 0);
	GPIOC->CRH = 0x30000000;  // Led on Pin 15
	
	//GPIOC->ODR |= (1<<15);
	while(1)
	{
	
		
	}
	return 0;
}

void EXTI0_IRQHandler(){
	// page 818 
	// Status register (USART_SR)
	// RXNE bit 5
	//GPIOC->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13
			//for(double i=0; i<1000;i++);		// delay

	if(EXTI->PR & 0x01){
		// Clear Intrrupt
		EXTI->PR = 0x01;
		GPIOC->ODR ^= (1<<15);
		/*for(long int xx=0; xx< 10000;xx++)
		{
		}*/
	}
	/*else
	{
		GPIOC->ODR &= ~(1<<15);
		for(long int xx=0; xx< 10000;xx++)
		{
		}
	}*/
	
}