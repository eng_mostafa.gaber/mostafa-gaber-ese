#include <stm32f10x.h>
#include "intrrupt.h"

#define fclk 8000000
#define baudrate 9600 
//#define AFIO		0
#define RXNE   	5			// 818
#define TC	   6			// 818
volatile unsigned char vData;
// Receive Application
int main()
{
	int x=0,j = 0;
	RCC->APB2ENR |= (1<<14) | (1<<0) | (1<<2) | (1<<3) | (1<<4); // and GPIOA, B, C
	NVIC->ISER[1] = (1<<5); // Enable USART1 Int. from NVIC
	//NVIC->ICER[1] = (1<<5); // Clear-enable
	//NVIC->ISPR[1] = (1<<5); // Set-pending
	//NVIC->ICPR[1] = (1<<5); // Clear-pending
	//NVIC->IABR[1] = (1<<5); // Active Bit
	USART1->CR1 |= (1<<13);
	USART1->CR1 &= ~(1<<12);
	// 8 bit word length
	// 1 stop bit
	USART1->CR2 &= ~(3<<12); // stop bit 1
	USART1->CR2 &= ~(1<<12); // stop bit 1
	
	USART1->CR1 |=  (1<<2); // TE, RE
	USART1->CR1|=(1<<5);
	
	USART1->BRR = fclk / baudrate;
	GPIOA->CRL = 0x00000003; // Pin 0 enable & Pin 5 
	GPIOA->CRH = 0x000004B0; // Page 161, 166
	GPIOB->CRH = 0x00300000;  // Led on Pin 13
	GPIOC->CRH = 0x30000000;  // Led on Pin 15
	//GPIOC->ODR |= (1<<13);	// set complimant 1 (0) to pin 13
	while(1)
	{
								//GPIOC->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13

		//USART1->DR = 'm';
		
		if(GPIOA->IDR & 0x0001) // Button on Pin 1
		{
			USART1->DR = 'm';
			while(!(USART1->SR & (1<<TC)));
			for(j=0; j<1500; j++);
		}
		else
		{
			USART1->DR = '\0';
			while(!(USART1->SR & (1<<TC)));
			for(j=0; j<1500; j++);
		}
		//GPIOC->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13
		if(vData =='m')
		{
					 //GPIOC->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13

			GPIOC->ODR &= ~(1<<15);	// set complimant 1 (0) to pin 13
			//GPIOB->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13
			for(int i=0; i<10000;i++);		// delay
		 //	GPIOC->ODR ^= (1<<13);	// set complimant 1 (0) to pin 13
		//	for(int i=0; i<10000;i++);		// delay
		}
		else
		{
			GPIOC->ODR |= (1<<15); // set 1 to pin 13
			//GPIOB->ODR |= (1<<13); // set 1 to pin 13
			for(int i=0; i<10000;i++);		// delay
		}
		
	}
	return 0;
}

void USART1_IRQHandler(){
	// page 818 
	// Status register (USART_SR)
	// RXNE bit 5
	//GPIOC->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13
			//for(double i=0; i<1000;i++);		// delay

	if(USART1->SR & (1<<5)){
		vData =USART1->DR&0x1FF;

	}
	
}