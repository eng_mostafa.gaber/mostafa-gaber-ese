#include <stm32f10x.h>
#define fclk 8000000
#define baudrate 9600 
#define AFIO		0
#define TC	6			// 818

// Transmit Application
int main()
{
	int x=0,j = 0;
	RCC->APB2ENR |= (1<<14) | (1<<AFIO) | (1<<2);
	USART1->CR1 |= (1<<13);
	USART1->CR1 &= ~(1<<12);
	// 8 bit word length
	// 1 stop bit
	USART1->CR2 &= ~(3<<12); // stop bit 1
	USART1->CR2 &= ~(1<<12); // stop bit 1
	
	USART1->CR1 |= (1<<3) | (1<<2); // TE, RE
	double baud = fclk / baudrate;
	USART1->BRR = baud;
	GPIOA->CRH = 0x000004B0; // Page 161
	char me[] = "mostafa";
	//int x=0;
	while(1)
	{
		//USART1->DR = 'm';
		for(x=0; x<7; x++)
		{
			USART1->DR = me[x];
		while(!(USART1->SR & (1<<TC)));
			for(j=0; j<1500; j++);
		}

		for(x=0; x<1500;x++);
	}
	return 0;
}