#include <stm32f10x.h>
#define fclk 8000000
#define baudrate 9600 
#define AFIO		0
#define RXNE	5			// 818

// Receive Application
int main()
{
	int x = 0;
	char rChar;
	RCC->APB2ENR |= (1<<14) | (1<<AFIO) | (1<<2);
	USART1->CR1 |= (1<<13);
	USART1->CR1 &= ~(1<<12);
	// 8 bit word length
	// 1 stop bit
	USART1->CR2 &= ~(3<<12); // stop bit 1
	USART1->CR2 &= ~(1<<12); // stop bit 1
	
	USART1->CR1 |= (1<<3) | (1<<2); // TE, RE
	double baud = fclk / baudrate;
	USART1->BRR = baud;
	GPIOA->CRH = 0x00300B40;  // Page 161
	while(1)
	{
		while(!(USART1->SR & (1<<RXNE)))
		{
			rChar = USART1->DR & 0x00FF;
			if(rChar != '\0')
			{
				GPIOA->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13
				for(int i=0; i<100000;i++);		// delay
			}
			else
			{
				GPIOA->ODR |= (1<<13); // set 1 to pin 13
				for(int i=0; i<100000;i++);		// delay
			}
		}
	}
	return 0;
}