#include <stm32f10x.h>
#include <stdio.h>
#include "configuration.h"

	
void Delay(long int second)
{
	//float delayMe = 4000000 * second;
	for(int i=0; i < second; i++);
}

void Button2Times8LEDonSequence()
{
		if(GPIOC->IDR & 0x4000){
			for(int twoTimes=0; twoTimes < 2; twoTimes++)
			{
				for(int LED=0; LED < 8; LED++)
				{
					for(int OneOn=0; OneOn < 8; OneOn++)
					{
						if(LED == OneOn)
						{
							GPIOA->ODR |= (1<<OneOn);	// set one LED on
						}
						else
						{
							GPIOA->ODR &= ~(1<<OneOn);	// set other LED's off
						}
					}
					Delay(500000);
				}
			}
			// when finish set LED 7 off
			GPIOA->ODR &= ~(1<<7);	// set other LED's off
			//Delay(10000000);
		}
}