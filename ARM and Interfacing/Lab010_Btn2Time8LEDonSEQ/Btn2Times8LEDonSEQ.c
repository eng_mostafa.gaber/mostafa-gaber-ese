#include <stm32f10x.h>
#include <stdio.h>
#include "configuration.h"

int main()
{
	// Configuration
	// enable register page 146
	RCC->APB2ENR |= (1<<2) | (1<<4);
	// All pin to be output with
	GPIOA->CRL = 0x33333333;
	GPIOC->CRH = 0x04000000; // Pin 14 input
	while(1)
	{
		Button2Times8LEDonSequence();
	}
	return 0;
}