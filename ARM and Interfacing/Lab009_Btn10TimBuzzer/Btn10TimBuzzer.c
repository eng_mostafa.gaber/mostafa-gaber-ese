#include <stm32f10x.h>
#include <stdio.h>
#include "configuration.h"

int main()
{
	// Configuration
	// enable register page 146
	RCC->APB2ENR |= (1<<2) | (1<<4);
	// All pin to be output with
	GPIOA->CRL = 0x00000033;
	GPIOC->CRH = 0x04000000; // Pin 14 input
	//GPIOC->CRH = 0x03000000; // Pin 14 input
	int POS=0;
	while(1)
	{
		//GPIOC-> ->IDR &= ~(0<<14);
		if(GPIOC->IDR & 0x4000){
			//for(int i=0; i<500000;i++);		// delay
			Delay(500000);
			POS++;
			if(POS == 1){
				GPIOA->ODR |= (1<<0);	// set led 1 on
			}
			if(POS == 10){
				GPIOA->ODR |= (1<<1);
			}
			else if(POS > 10)
			{
				GPIOA->ODR &= ~(1<<1);
				GPIOA->ODR &= ~(1<<0);	// set led 1 off
				POS = 0;
			}
		}
	}
	return 0;
}