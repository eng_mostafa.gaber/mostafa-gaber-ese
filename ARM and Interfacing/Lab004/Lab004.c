#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	// Configuration
	// enable register page 146
	RCC->APB2ENR |= (1<<2);
	// All pin to be output with
	GPIOA->CRL = 0x33333333;
	GPIOA->CRH = 0x33333333;
	while(1)
	{
		//loop
		GPIOA->ODR = 0x0000;	// set all pin = 0
		for(int i=0; i<100000;i++);		// delay
		GPIOA->ODR = 0xffff;	// set all pin = 1
		for(int i=0; i<100000;i++);		// delay
	}
	return 0;
}