#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	// Configuration
	// Enable clock for GPIOC from APB2ENR RCC(reset clock and control module)
	RCC->APB2ENR |= (1<<4);
	// PinC13 to be output with high speed [3] 0x40011004
	GPIOC->CRH = 0x00300000;
	while(1)
	{
		// loop
		GPIOC->ODR |= (1<<13);		// set pin 13 = 1
		for(int i=0; i<100000;i++);		// delay
		GPIOC->ODR &= ~(1<<13);		// reset pin C13 = 0
		for(int i=0; i<100000;i++);		// delay
	}
	return 0;
}