#include <stm32f10x.h>
#define fclk 8000000
#define baudrate 9600 
//#define AFIO 0
#define TC	6			// 818

// Transmit Application
int main()
{
	int x=0,j = 0;
	RCC->APB2ENR |= (1<<14) | (1<<0) | (1<<2) | (1<<3); // Enable Port A, B, C & D Page 113
	USART1->CR1 |= (1<<13);
	USART1->CR1 &= ~(1<<12);
	// 8 bit word length
	// 1 stop bit
	USART1->CR2 &= ~(3<<12); // stop bit 1
	USART1->CR2 &= ~(1<<12); // stop bit 1
	
	USART1->CR1 |= (1<<3) | (1<<2); // TE, RE
	double baud = fclk / baudrate;
	USART1->BRR = baud;
	GPIOA->CRH = 0x000004B0; // Page 161, 166
	GPIOB->CRH = 0x00004444; // for Pin 8, 9, 10 & 11
	//char me[] = "mostafa";
	while(1)
	{
		//USART1->DR = 'm';
		/*for(x=0; x<7; x++)
		{
			USART1->DR = me[x];
			while(!(USART1->SR & (1<<TC)));
			for(j=0; j<1500; j++);
		}*/
		if(GPIOB->IDR & 0x0100) // Button on Pin 8
		{
			USART1->DR = 'm';
			while(!(USART1->SR & (1<<TC)));
			//for(j=0; j<1500; j++);
			//GPIOB->IDR |= 0x0100; // set 1 to pin 8
		}
		else if(GPIOB->IDR & 0x0200) // Button on Pin 9
		{
			USART1->DR = 'n';
			while(!(USART1->SR & (1<<TC)));
			//for(j=0; j<1500; j++);
			//GPIOB->IDR |= 0x0200; // set 1 to pin 9
		}
		else if(GPIOB->IDR & 0x0400) // Button on Pin 10
		{
			USART1->DR = 'x';
			while(!(USART1->SR & (1<<TC)));
			//for(j=0; j<1500; j++);
			//GPIOB->IDR |= 0x0400; // set 1 to pin 10
		}
    else if(GPIOB->IDR & 0x0800) // Button on Pin 11
		{
			USART1->DR = 'y';
			while(!(USART1->SR & (1<<TC)));
			//for(j=0; j<1500; j++);
			//GPIOB->IDR |= 0x0800; // set 1 to pin 11
		}
		/*else
		{
			USART1->DR = 'k';
			while(!(USART1->SR & (1<<TC)));
			for(j=0; j<1500; j++);
		}*/
		
		for(x=0; x<1500;x++);
	}
	return 0;
}