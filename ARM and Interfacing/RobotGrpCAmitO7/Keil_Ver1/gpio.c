#include "gpio.h"
// A
// B

// C
void Clock_Enable(GPIO_Typedef* GPIO){
	if(GPIO == GPIOA){
		RCC_APB2ENR |= RCC_APB2ENR_IOPAEN;
	}
	else if(GPIO == GPIOB){
		RCC_APB2ENR |= RCC_APB2ENR_IOPBEN;
	}
	else if(GPIO == GPIOC){
		RCC_APB2ENR |= RCC_APB2ENR_IOPCEN;
	}
	else if(GPIO == GPIOD){
		RCC_APB2ENR |= RCC_APB2ENR_IOPDEN;
	}
}

// D
void Delay(long int second)
{
	//float delayMe = 4000000 * second;
	for(int i=0; i < second; i++);
}

// E // F // G // H
// I
void InitPort(GPIO_Typedef* GPIO, long value, bool CtrlHigh){
	Clock_Enable(GPIO);
	if(CtrlHigh)
		GPIO->CRH = value;
	else
		GPIO->CRL = value;
}

void OutPort(GPIO_Typedef* GPIO, long value, bool Reset){
	if(Reset)
		GPIO->ODR = value;
	else
		GPIO->ODR |= value;
}
