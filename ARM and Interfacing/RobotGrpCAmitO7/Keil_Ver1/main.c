#include <stm32f10x.h>
#define fclk 8000000
#define baudrate 9600 
//#define AFIO		0
#define RXNE	5			// 818
#define TC	   6			// 818

// Receive Application
int main()
{
	int x=0,j = 0;
	unsigned char rChar = '\0';
	RCC->APB2ENR |= (1<<14) | (1<<0) | (1<<2) | (1<<3) | (1<<4);
	// Page 795
	USART1->CR1 |= (1<<13);
	USART1->CR1 &= ~(1<<12);
	// 8 bit word length
	// 1 stop bit
	USART1->CR2 &= ~(3<<12); // stop bit 1
	USART1->CR2 &= ~(1<<12); // stop bit 1
	
	USART1->CR1 |= (1<<3) | (1<<2); // TE, RE
	double baud = fclk / baudrate;
	USART1->BRR = baud;
	GPIOA->CRH = 0x000004B0;  // Page 161
	GPIOB->CRH = 0x33330000; // for Pin 12, 13, 14 & 15
	GPIOC->CRH = 0x00300000; // for Pin 13
	
	// Active Pin PB0 & PB1
	GPIOB->CRL = 0x00000033;
	GPIOB->ODR |= (1<<0) | (1<<1);
	/*
	GPIOB->ODR ^= (1<<12);	// toggle => set complimant 1 (0) to pin 13
	GPIOB->ODR ^= (1<<14);	// toggle => set complimant 1 (0) to pin 13
	*/
	char me[] = "Done...\n";
	while(1)
	{
		/*
					
					
					GPIOB->ODR |= (1<<12);
					GPIOB->ODR &= ~(1<<13);
					GPIOB->ODR &= ~(1<<14);
					GPIOB->ODR |= (1<<15);
					
					GPIOC->ODR ^= (1<<13);
			
		USART1->DR = 'O';
		while(!(USART1->SR & (1<<TC)));
		for(j=0; j<150000; j++);
			
		while(!(USART1->SR & (1<<RXNE)))
		*/		
		while((USART1->SR & (1<<RXNE)))
		{
			/*if(USART1->SR & (1<<5)){
				vData =USART1->DR&0x1FF;
			}*/
			rChar = USART1->DR & 0x00FF;
			//rChar = (USART1->rd & 0x00FF);
			if(rChar != '\0')
			{
				if(rChar == 'm')
				{
					// Move forward
					GPIOB->ODR &= ~(1<<12);
					GPIOB->ODR |= (1<<13);
					GPIOB->ODR |= (1<<14);
					GPIOB->ODR &= ~(1<<15);
					
					//GPIOC->ODR |= (1<<13);
				}
				else if(rChar == 'n')
				{
					// Move back
					GPIOB->ODR |= (1<<12);
					GPIOB->ODR &= ~(1<<13);
					GPIOB->ODR &= ~(1<<14);
					GPIOB->ODR |= (1<<15);
					
					GPIOC->ODR &= ~(1<<13);
				}
				else if(rChar == 'x')
				{
					// Move Right (Left wheel)
					GPIOB->ODR &= ~(1<<12);
					GPIOB->ODR |= (1<<13);
					GPIOB->ODR &= ~(1<<14);
					GPIOB->ODR &= ~(1<<15);
				}
				else if(rChar == 'y')
				{
					// Move Left (Right wheel)
					GPIOB->ODR &= ~(1<<12);
					GPIOB->ODR &= ~(1<<13);
					GPIOB->ODR |= (1<<14);
					GPIOB->ODR &= ~(1<<15);
				}
				else if(rChar == 'r')
				{
					// Move back right (right wheel)
					GPIOB->ODR &= ~(1<<12);
					GPIOB->ODR &= ~(1<<13);
					GPIOB->ODR &= ~(1<<14);
					GPIOB->ODR |= (1<<15);
				}
				else if(rChar == 'l')
				{
					// Move back left (left wheel)
					GPIOB->ODR |= (1<<12);
					GPIOB->ODR &= ~(1<<13);
					GPIOB->ODR &= ~(1<<14);
					GPIOB->ODR &= ~(1<<15);
				}
				else if(rChar == 't')
				{
					// Move back left (left wheel)
					GPIOB->ODR &= ~(1<<12);
					GPIOB->ODR &= ~(1<<13);
					GPIOB->ODR &= ~(1<<14);
					GPIOB->ODR &= ~(1<<15);
				}
				rChar = '\0';
				for(x=0; x<7; x++)
				{
					USART1->DR = me[x];
					while(!(USART1->SR & (1<<TC)))
					{
					}
					for(j=0; j<1500; j++);
				}
			}
		}
	}
	return 0;
}