#ifndef GPIO_H_INCLUDED
#define GPIO_H_INCLUDED

typedef struct {
	unsigned long CRL;
	unsigned long CRH;
	unsigned long IDR;
	unsigned long ODR;
	unsigned long BSRR;
  unsigned long BRR;
  unsigned long LCKR;
} GPIO_Typedef;


// Define the registers we need to do a pin toggle
#define ADDR(x)     (*((unsigned long*)(x)))

// These registers define clocks, RCC (Reset and clock control)
#define RCC_BASE        0x40021000
#define RCC_APB2ENR     ADDR(RCC_BASE + 0x18)

#define PERIPH_BASE           (0x40000000) /*!< Peripheral base address in the alias region */

/*!< Peripheral memory map */
#define APB1PERIPH_BASE       PERIPH_BASE         		 //APB1 'BUS for Peripherals' base address
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000) //APB2 Peripheral base address
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)     
#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)  
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)  
#define GPIOC_BASE            (APB2PERIPH_BASE + 0x1000) 
#define GPIOD_BASE            (APB2PERIPH_BASE + 0x1400)

#define GPIOA									((GPIO_Typedef*) GPIOA_BASE)
#define GPIOB									((GPIO_Typedef*) GPIOB_BASE)
#define GPIOC									((GPIO_Typedef*) GPIOC_BASE)
#define GPIOD									((GPIO_Typedef*) GPIOD_BASE)


/******************  Bit definition for RCC_APB2ENR register  *****************/
#define  RCC_APB2ENR_AFIOEN                  ((unsigned int)0x00000001)         /*!< Alternate Function I/O clock enable */
#define  RCC_APB2ENR_IOPAEN                  ((unsigned int)0x00000004)         /*!< I/O port A clock enable */
#define  RCC_APB2ENR_IOPBEN                  ((unsigned int)0x00000008)         /*!< I/O port B clock enable */
#define  RCC_APB2ENR_IOPCEN                  ((unsigned int)0x00000010)         /*!< I/O port C clock enable */
#define  RCC_APB2ENR_IOPDEN                  ((unsigned int)0x00000020)         /*!< I/O port D clock enable */
#define  RCC_APB2ENR_ADC1EN                  ((unsigned int)0x00000200)         /*!< ADC 1 interface clock enable */

/******************************************************************************/
/*                                                                            */
/*                General Purpose and Alternate Function I/O                  */
/*                                                                            */
/******************************************************************************/

/*******************  Bit definition for GPIO_CRL register  *******************/
#define  GPIO_CRL_MODE                       ((unsigned int)0x33333333)        /*!< Port x mode bits */

#define  GPIO_CRL_MODE0                      ((unsigned int)0x00000003)        /*!< MODE0[1:0] bits (Port x mode bits, pin 0) */
#define  GPIO_CRL_MODE0_0                    ((unsigned int)0x00000001)        /*!< Bit 0 */
#define  GPIO_CRL_MODE0_1                    ((unsigned int)0x00000002)        /*!< Bit 1 */

#define  GPIO_CRL_MODE1                      ((unsigned int)0x00000030)        /*!< MODE1[1:0] bits (Port x mode bits, pin 1) */
#define  GPIO_CRL_MODE1_0                    ((unsigned int)0x00000010)        /*!< Bit 0 */
#define  GPIO_CRL_MODE1_1                    ((unsigned int)0x00000020)        /*!< Bit 1 */

#define  GPIO_CRL_MODE2                      ((unsigned int)0x00000300)        /*!< MODE2[1:0] bits (Port x mode bits, pin 2) */
#define  GPIO_CRL_MODE2_0                    ((unsigned int)0x00000100)        /*!< Bit 0 */
#define  GPIO_CRL_MODE2_1                    ((unsigned int)0x00000200)        /*!< Bit 1 */

#define  GPIO_CRL_MODE3                      ((unsigned int)0x00003000)        /*!< MODE3[1:0] bits (Port x mode bits, pin 3) */
#define  GPIO_CRL_MODE3_0                    ((unsigned int)0x00001000)        /*!< Bit 0 */
#define  GPIO_CRL_MODE3_1                    ((unsigned int)0x00002000)        /*!< Bit 1 */

#define  GPIO_CRL_MODE4                      ((unsigned int)0x00030000)        /*!< MODE4[1:0] bits (Port x mode bits, pin 4) */
#define  GPIO_CRL_MODE4_0                    ((unsigned int)0x00010000)        /*!< Bit 0 */
#define  GPIO_CRL_MODE4_1                    ((unsigned int)0x00020000)        /*!< Bit 1 */

#define  GPIO_CRL_MODE5                      ((unsigned int)0x00300000)        /*!< MODE5[1:0] bits (Port x mode bits, pin 5) */
#define  GPIO_CRL_MODE5_0                    ((unsigned int)0x00100000)        /*!< Bit 0 */
#define  GPIO_CRL_MODE5_1                    ((unsigned int)0x00200000)        /*!< Bit 1 */

#define  GPIO_CRL_MODE6                      ((unsigned int)0x03000000)        /*!< MODE6[1:0] bits (Port x mode bits, pin 6) */
#define  GPIO_CRL_MODE6_0                    ((unsigned int)0x01000000)        /*!< Bit 0 */
#define  GPIO_CRL_MODE6_1                    ((unsigned int)0x02000000)        /*!< Bit 1 */

#define  GPIO_CRL_MODE7                      ((unsigned int)0x30000000)        /*!< MODE7[1:0] bits (Port x mode bits, pin 7) */
#define  GPIO_CRL_MODE7_0                    ((unsigned int)0x10000000)        /*!< Bit 0 */
#define  GPIO_CRL_MODE7_1                    ((unsigned int)0x20000000)        /*!< Bit 1 */

#define  GPIO_CRL_CNF                        ((unsigned int)0xCCCCCCCC)        /*!< Port x configuration bits */

#define  GPIO_CRL_CNF0                       ((unsigned int)0x0000000C)        /*!< CNF0[1:0] bits (Port x configuration bits, pin 0) */
#define  GPIO_CRL_CNF0_0                     ((unsigned int)0x00000004)        /*!< Bit 0 */
#define  GPIO_CRL_CNF0_1                     ((unsigned int)0x00000008)        /*!< Bit 1 */

#define  GPIO_CRL_CNF1                       ((unsigned int)0x000000C0)        /*!< CNF1[1:0] bits (Port x configuration bits, pin 1) */
#define  GPIO_CRL_CNF1_0                     ((unsigned int)0x00000040)        /*!< Bit 0 */
#define  GPIO_CRL_CNF1_1                     ((unsigned int)0x00000080)        /*!< Bit 1 */

#define  GPIO_CRL_CNF2                       ((unsigned int)0x00000C00)        /*!< CNF2[1:0] bits (Port x configuration bits, pin 2) */
#define  GPIO_CRL_CNF2_0                     ((unsigned int)0x00000400)        /*!< Bit 0 */
#define  GPIO_CRL_CNF2_1                     ((unsigned int)0x00000800)        /*!< Bit 1 */

#define  GPIO_CRL_CNF3                       ((unsigned int)0x0000C000)        /*!< CNF3[1:0] bits (Port x configuration bits, pin 3) */
#define  GPIO_CRL_CNF3_0                     ((unsigned int)0x00004000)        /*!< Bit 0 */
#define  GPIO_CRL_CNF3_1                     ((unsigned int)0x00008000)        /*!< Bit 1 */

#define  GPIO_CRL_CNF4                       ((unsigned int)0x000C0000)        /*!< CNF4[1:0] bits (Port x configuration bits, pin 4) */
#define  GPIO_CRL_CNF4_0                     ((unsigned int)0x00040000)        /*!< Bit 0 */
#define  GPIO_CRL_CNF4_1                     ((unsigned int)0x00080000)        /*!< Bit 1 */

#define  GPIO_CRL_CNF5                       ((unsigned int)0x00C00000)        /*!< CNF5[1:0] bits (Port x configuration bits, pin 5) */
#define  GPIO_CRL_CNF5_0                     ((unsigned int)0x00400000)        /*!< Bit 0 */
#define  GPIO_CRL_CNF5_1                     ((unsigned int)0x00800000)        /*!< Bit 1 */

#define  GPIO_CRL_CNF6                       ((unsigned int)0x0C000000)        /*!< CNF6[1:0] bits (Port x configuration bits, pin 6) */
#define  GPIO_CRL_CNF6_0                     ((unsigned int)0x04000000)        /*!< Bit 0 */
#define  GPIO_CRL_CNF6_1                     ((unsigned int)0x08000000)        /*!< Bit 1 */

#define  GPIO_CRL_CNF7                       ((unsigned int)0xC0000000)        /*!< CNF7[1:0] bits (Port x configuration bits, pin 7) */
#define  GPIO_CRL_CNF7_0                     ((unsigned int)0x40000000)        /*!< Bit 0 */
#define  GPIO_CRL_CNF7_1                     ((unsigned int)0x80000000)        /*!< Bit 1 */

/*******************  Bit definition for GPIO_CRH register  *******************/
#define  GPIO_CRH_MODE                       ((unsigned int)0x33333333)        /*!< Port x mode bits */

#define  GPIO_CRH_MODE8                      ((unsigned int)0x00000003)        /*!< MODE8[1:0] bits (Port x mode bits, pin 8) */
#define  GPIO_CRH_MODE8_0                    ((unsigned int)0x00000001)        /*!< Bit 0 */
#define  GPIO_CRH_MODE8_1                    ((unsigned int)0x00000002)        /*!< Bit 1 */

#define  GPIO_CRH_MODE9                      ((unsigned int)0x00000030)        /*!< MODE9[1:0] bits (Port x mode bits, pin 9) */
#define  GPIO_CRH_MODE9_0                    ((unsigned int)0x00000010)        /*!< Bit 0 */
#define  GPIO_CRH_MODE9_1                    ((unsigned int)0x00000020)        /*!< Bit 1 */

#define  GPIO_CRH_MODE10                     ((unsigned int)0x00000300)        /*!< MODE10[1:0] bits (Port x mode bits, pin 10) */
#define  GPIO_CRH_MODE10_0                   ((unsigned int)0x00000100)        /*!< Bit 0 */
#define  GPIO_CRH_MODE10_1                   ((unsigned int)0x00000200)        /*!< Bit 1 */

#define  GPIO_CRH_MODE11                     ((unsigned int)0x00003000)        /*!< MODE11[1:0] bits (Port x mode bits, pin 11) */
#define  GPIO_CRH_MODE11_0                   ((unsigned int)0x00001000)        /*!< Bit 0 */
#define  GPIO_CRH_MODE11_1                   ((unsigned int)0x00002000)        /*!< Bit 1 */

#define  GPIO_CRH_MODE12                     ((unsigned int)0x00030000)        /*!< MODE12[1:0] bits (Port x mode bits, pin 12) */
#define  GPIO_CRH_MODE12_0                   ((unsigned int)0x00010000)        /*!< Bit 0 */
#define  GPIO_CRH_MODE12_1                   ((unsigned int)0x00020000)        /*!< Bit 1 */

#define  GPIO_CRH_MODE13                     ((unsigned int)0x00300000)        /*!< MODE13[1:0] bits (Port x mode bits, pin 13) */
#define  GPIO_CRH_MODE13_0                   ((unsigned int)0x00100000)        /*!< Bit 0 */
#define  GPIO_CRH_MODE13_1                   ((unsigned int)0x00200000)        /*!< Bit 1 */

#define  GPIO_CRH_MODE14                     ((unsigned int)0x03000000)        /*!< MODE14[1:0] bits (Port x mode bits, pin 14) */
#define  GPIO_CRH_MODE14_0                   ((unsigned int)0x01000000)        /*!< Bit 0 */
#define  GPIO_CRH_MODE14_1                   ((unsigned int)0x02000000)        /*!< Bit 1 */

#define  GPIO_CRH_MODE15                     ((unsigned int)0x30000000)        /*!< MODE15[1:0] bits (Port x mode bits, pin 15) */
#define  GPIO_CRH_MODE15_0                   ((unsigned int)0x10000000)        /*!< Bit 0 */
#define  GPIO_CRH_MODE15_1                   ((unsigned int)0x20000000)        /*!< Bit 1 */

#define  GPIO_CRH_CNF                        ((unsigned int)0xCCCCCCCC)        /*!< Port x configuration bits */

#define  GPIO_CRH_CNF8                       ((unsigned int)0x0000000C)        /*!< CNF8[1:0] bits (Port x configuration bits, pin 8) */
#define  GPIO_CRH_CNF8_0                     ((unsigned int)0x00000004)        /*!< Bit 0 */
#define  GPIO_CRH_CNF8_1                     ((unsigned int)0x00000008)        /*!< Bit 1 */

#define  GPIO_CRH_CNF9                       ((unsigned int)0x000000C0)        /*!< CNF9[1:0] bits (Port x configuration bits, pin 9) */
#define  GPIO_CRH_CNF9_0                     ((unsigned int)0x00000040)        /*!< Bit 0 */
#define  GPIO_CRH_CNF9_1                     ((unsigned int)0x00000080)        /*!< Bit 1 */

#define  GPIO_CRH_CNF10                      ((unsigned int)0x00000C00)        /*!< CNF10[1:0] bits (Port x configuration bits, pin 10) */
#define  GPIO_CRH_CNF10_0                    ((unsigned int)0x00000400)        /*!< Bit 0 */
#define  GPIO_CRH_CNF10_1                    ((unsigned int)0x00000800)        /*!< Bit 1 */

#define  GPIO_CRH_CNF11                      ((unsigned int)0x0000C000)        /*!< CNF11[1:0] bits (Port x configuration bits, pin 11) */
#define  GPIO_CRH_CNF11_0                    ((unsigned int)0x00004000)        /*!< Bit 0 */
#define  GPIO_CRH_CNF11_1                    ((unsigned int)0x00008000)        /*!< Bit 1 */

#define  GPIO_CRH_CNF12                      ((unsigned int)0x000C0000)        /*!< CNF12[1:0] bits (Port x configuration bits, pin 12) */
#define  GPIO_CRH_CNF12_0                    ((unsigned int)0x00040000)        /*!< Bit 0 */
#define  GPIO_CRH_CNF12_1                    ((unsigned int)0x00080000)        /*!< Bit 1 */

#define  GPIO_CRH_CNF13                      ((unsigned int)0x00C00000)        /*!< CNF13[1:0] bits (Port x configuration bits, pin 13) */
#define  GPIO_CRH_CNF13_0                    ((unsigned int)0x00400000)        /*!< Bit 0 */
#define  GPIO_CRH_CNF13_1                    ((unsigned int)0x00800000)        /*!< Bit 1 */

#define  GPIO_CRH_CNF14                      ((unsigned int)0x0C000000)        /*!< CNF14[1:0] bits (Port x configuration bits, pin 14) */
#define  GPIO_CRH_CNF14_0                    ((unsigned int)0x04000000)        /*!< Bit 0 */
#define  GPIO_CRH_CNF14_1                    ((unsigned int)0x08000000)        /*!< Bit 1 */

#define  GPIO_CRH_CNF15                      ((unsigned int)0xC0000000)        /*!< CNF15[1:0] bits (Port x configuration bits, pin 15) */
#define  GPIO_CRH_CNF15_0                    ((unsigned int)0x40000000)        /*!< Bit 0 */
#define  GPIO_CRH_CNF15_1                    ((unsigned int)0x80000000)        /*!< Bit 1 */

/*!<******************  Bit definition for GPIO_IDR register  *******************/
#define GPIO_IDR_IDR0                        ((unsigned short int)0x0001)            /*!< Port input data, bit 0 */
#define GPIO_IDR_IDR1                        ((unsigned short int)0x0002)            /*!< Port input data, bit 1 */
#define GPIO_IDR_IDR2                        ((unsigned short int)0x0004)            /*!< Port input data, bit 2 */
#define GPIO_IDR_IDR3                        ((unsigned short int)0x0008)            /*!< Port input data, bit 3 */
#define GPIO_IDR_IDR4                        ((unsigned short int)0x0010)            /*!< Port input data, bit 4 */
#define GPIO_IDR_IDR5                        ((unsigned short int)0x0020)            /*!< Port input data, bit 5 */
#define GPIO_IDR_IDR6                        ((unsigned short int)0x0040)            /*!< Port input data, bit 6 */
#define GPIO_IDR_IDR7                        ((unsigned short int)0x0080)            /*!< Port input data, bit 7 */
#define GPIO_IDR_IDR8                        ((unsigned short int)0x0100)            /*!< Port input data, bit 8 */
#define GPIO_IDR_IDR9                        ((unsigned short int)0x0200)            /*!< Port input data, bit 9 */
#define GPIO_IDR_IDR10                       ((unsigned short int)0x0400)            /*!< Port input data, bit 10 */
#define GPIO_IDR_IDR11                       ((unsigned short int)0x0800)            /*!< Port input data, bit 11 */
#define GPIO_IDR_IDR12                       ((unsigned short int)0x1000)            /*!< Port input data, bit 12 */
#define GPIO_IDR_IDR13                       ((unsigned short int)0x2000)            /*!< Port input data, bit 13 */
#define GPIO_IDR_IDR14                       ((unsigned short int)0x4000)            /*!< Port input data, bit 14 */
#define GPIO_IDR_IDR15                       ((unsigned short int)0x8000)            /*!< Port input data, bit 15 */

/*******************  Bit definition for GPIO_ODR register  *******************/
#define GPIO_ODR_ODR0                        ((unsigned short int)0x0001)            /*!< Port output data, bit 0 */
#define GPIO_ODR_ODR1                        ((unsigned short int)0x0002)            /*!< Port output data, bit 1 */
#define GPIO_ODR_ODR2                        ((unsigned short int)0x0004)            /*!< Port output data, bit 2 */
#define GPIO_ODR_ODR3                        ((unsigned short int)0x0008)            /*!< Port output data, bit 3 */
#define GPIO_ODR_ODR4                        ((unsigned short int)0x0010)            /*!< Port output data, bit 4 */
#define GPIO_ODR_ODR5                        ((unsigned short int)0x0020)            /*!< Port output data, bit 5 */
#define GPIO_ODR_ODR6                        ((unsigned short int)0x0040)            /*!< Port output data, bit 6 */
#define GPIO_ODR_ODR7                        ((unsigned short int)0x0080)            /*!< Port output data, bit 7 */
#define GPIO_ODR_ODR8                        ((unsigned short int)0x0100)            /*!< Port output data, bit 8 */
#define GPIO_ODR_ODR9                        ((unsigned short int)0x0200)            /*!< Port output data, bit 9 */
#define GPIO_ODR_ODR10                       ((unsigned short int)0x0400)            /*!< Port output data, bit 10 */
#define GPIO_ODR_ODR11                       ((unsigned short int)0x0800)            /*!< Port output data, bit 11 */
#define GPIO_ODR_ODR12                       ((unsigned short int)0x1000)            /*!< Port output data, bit 12 */
#define GPIO_ODR_ODR13                       ((unsigned short int)0x2000)            /*!< Port output data, bit 13 */
#define GPIO_ODR_ODR14                       ((unsigned short int)0x4000)            /*!< Port output data, bit 14 */
#define GPIO_ODR_ODR15                       ((unsigned short int)0x8000)            /*!< Port output data, bit 15 */
#define GPIO_ODR_ODRZERO                     ((unsigned short int)0x0000)            /*!< Port output data, Zero output */

/******************  Bit definition for GPIO_BSRR register  *******************/
#define GPIO_BSRR_BS0                        ((unsigned int)0x00000001)        /*!< Port x Set bit 0 */
#define GPIO_BSRR_BS1                        ((unsigned int)0x00000002)        /*!< Port x Set bit 1 */
#define GPIO_BSRR_BS2                        ((unsigned int)0x00000004)        /*!< Port x Set bit 2 */
#define GPIO_BSRR_BS3                        ((unsigned int)0x00000008)        /*!< Port x Set bit 3 */
#define GPIO_BSRR_BS4                        ((unsigned int)0x00000010)        /*!< Port x Set bit 4 */
#define GPIO_BSRR_BS5                        ((unsigned int)0x00000020)        /*!< Port x Set bit 5 */
#define GPIO_BSRR_BS6                        ((unsigned int)0x00000040)        /*!< Port x Set bit 6 */
#define GPIO_BSRR_BS7                        ((unsigned int)0x00000080)        /*!< Port x Set bit 7 */
#define GPIO_BSRR_BS8                        ((unsigned int)0x00000100)        /*!< Port x Set bit 8 */
#define GPIO_BSRR_BS9                        ((unsigned int)0x00000200)        /*!< Port x Set bit 9 */
#define GPIO_BSRR_BS10                       ((unsigned int)0x00000400)        /*!< Port x Set bit 10 */
#define GPIO_BSRR_BS11                       ((unsigned int)0x00000800)        /*!< Port x Set bit 11 */
#define GPIO_BSRR_BS12                       ((unsigned int)0x00001000)        /*!< Port x Set bit 12 */
#define GPIO_BSRR_BS13                       ((unsigned int)0x00002000)        /*!< Port x Set bit 13 */
#define GPIO_BSRR_BS14                       ((unsigned int)0x00004000)        /*!< Port x Set bit 14 */
#define GPIO_BSRR_BS15                       ((unsigned int)0x00008000)        /*!< Port x Set bit 15 */

#define GPIO_BSRR_BR0                        ((unsigned int)0x00010000)        /*!< Port x Reset bit 0 */
#define GPIO_BSRR_BR1                        ((unsigned int)0x00020000)        /*!< Port x Reset bit 1 */
#define GPIO_BSRR_BR2                        ((unsigned int)0x00040000)        /*!< Port x Reset bit 2 */
#define GPIO_BSRR_BR3                        ((unsigned int)0x00080000)        /*!< Port x Reset bit 3 */
#define GPIO_BSRR_BR4                        ((unsigned int)0x00100000)        /*!< Port x Reset bit 4 */
#define GPIO_BSRR_BR5                        ((unsigned int)0x00200000)        /*!< Port x Reset bit 5 */
#define GPIO_BSRR_BR6                        ((unsigned int)0x00400000)        /*!< Port x Reset bit 6 */
#define GPIO_BSRR_BR7                        ((unsigned int)0x00800000)        /*!< Port x Reset bit 7 */
#define GPIO_BSRR_BR8                        ((unsigned int)0x01000000)        /*!< Port x Reset bit 8 */
#define GPIO_BSRR_BR9                        ((unsigned int)0x02000000)        /*!< Port x Reset bit 9 */
#define GPIO_BSRR_BR10                       ((unsigned int)0x04000000)        /*!< Port x Reset bit 10 */
#define GPIO_BSRR_BR11                       ((unsigned int)0x08000000)        /*!< Port x Reset bit 11 */
#define GPIO_BSRR_BR12                       ((unsigned int)0x10000000)        /*!< Port x Reset bit 12 */
#define GPIO_BSRR_BR13                       ((unsigned int)0x20000000)        /*!< Port x Reset bit 13 */
#define GPIO_BSRR_BR14                       ((unsigned int)0x40000000)        /*!< Port x Reset bit 14 */
#define GPIO_BSRR_BR15                       ((unsigned int)0x80000000)        /*!< Port x Reset bit 15 */

/*******************  Bit definition for GPIO_BRR register  *******************/
#define GPIO_BRR_BR0                         ((unsigned short int)0x0001)            /*!< Port x Reset bit 0 */
#define GPIO_BRR_BR1                         ((unsigned short int)0x0002)            /*!< Port x Reset bit 1 */
#define GPIO_BRR_BR2                         ((unsigned short int)0x0004)            /*!< Port x Reset bit 2 */
#define GPIO_BRR_BR3                         ((unsigned short int)0x0008)            /*!< Port x Reset bit 3 */
#define GPIO_BRR_BR4                         ((unsigned short int)0x0010)            /*!< Port x Reset bit 4 */
#define GPIO_BRR_BR5                         ((unsigned short int)0x0020)            /*!< Port x Reset bit 5 */
#define GPIO_BRR_BR6                         ((unsigned short int)0x0040)            /*!< Port x Reset bit 6 */
#define GPIO_BRR_BR7                         ((unsigned short int)0x0080)            /*!< Port x Reset bit 7 */
#define GPIO_BRR_BR8                         ((unsigned short int)0x0100)            /*!< Port x Reset bit 8 */
#define GPIO_BRR_BR9                         ((unsigned short int)0x0200)            /*!< Port x Reset bit 9 */
#define GPIO_BRR_BR10                        ((unsigned short int)0x0400)            /*!< Port x Reset bit 10 */
#define GPIO_BRR_BR11                        ((unsigned short int)0x0800)            /*!< Port x Reset bit 11 */
#define GPIO_BRR_BR12                        ((unsigned short int)0x1000)            /*!< Port x Reset bit 12 */
#define GPIO_BRR_BR13                        ((unsigned short int)0x2000)            /*!< Port x Reset bit 13 */
#define GPIO_BRR_BR14                        ((unsigned short int)0x4000)            /*!< Port x Reset bit 14 */
#define GPIO_BRR_BR15                        ((unsigned short int)0x8000)            /*!< Port x Reset bit 15 */

/******************  Bit definition for GPIO_LCKR register  *******************/
#define GPIO_LCKR_LCK0                       ((unsigned int)0x00000001)        /*!< Port x Lock bit 0 */
#define GPIO_LCKR_LCK1                       ((unsigned int)0x00000002)        /*!< Port x Lock bit 1 */
#define GPIO_LCKR_LCK2                       ((unsigned int)0x00000004)        /*!< Port x Lock bit 2 */
#define GPIO_LCKR_LCK3                       ((unsigned int)0x00000008)        /*!< Port x Lock bit 3 */
#define GPIO_LCKR_LCK4                       ((unsigned int)0x00000010)        /*!< Port x Lock bit 4 */
#define GPIO_LCKR_LCK5                       ((unsigned int)0x00000020)        /*!< Port x Lock bit 5 */
#define GPIO_LCKR_LCK6                       ((unsigned int)0x00000040)        /*!< Port x Lock bit 6 */
#define GPIO_LCKR_LCK7                       ((unsigned int)0x00000080)        /*!< Port x Lock bit 7 */
#define GPIO_LCKR_LCK8                       ((unsigned int)0x00000100)        /*!< Port x Lock bit 8 */
#define GPIO_LCKR_LCK9                       ((unsigned int)0x00000200)        /*!< Port x Lock bit 9 */
#define GPIO_LCKR_LCK10                      ((unsigned int)0x00000400)        /*!< Port x Lock bit 10 */
#define GPIO_LCKR_LCK11                      ((unsigned int)0x00000800)        /*!< Port x Lock bit 11 */
#define GPIO_LCKR_LCK12                      ((unsigned int)0x00001000)        /*!< Port x Lock bit 12 */
#define GPIO_LCKR_LCK13                      ((unsigned int)0x00002000)        /*!< Port x Lock bit 13 */
#define GPIO_LCKR_LCK14                      ((unsigned int)0x00004000)        /*!< Port x Lock bit 14 */
#define GPIO_LCKR_LCK15                      ((unsigned int)0x00008000)        /*!< Port x Lock bit 15 */
#define GPIO_LCKR_LCKK                       ((unsigned int)0x00010000)        /*!< Lock key */

/*----------------------------------------------------------------------------*/

typedef enum { false, true } bool;
// A
// B
// C
void Clock_Enable(GPIO_Typedef* GPIO);
// D
void Delay(long int second);
// E // F // G // H
// I
void InitPort(GPIO_Typedef* GPIO, long value, bool CtrlHigh);
// J // K // L

void OutPort(GPIO_Typedef* GPIO, long value, bool Reset);

#endif // GPIO_H_INCLUDED
