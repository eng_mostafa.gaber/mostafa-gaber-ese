/*
Author: Mohamed Saied
Date:  4/2/2019

*This is a Demo for GPIO in STM32F103
*STEPS:
			1)Enable Clock for GPIOC
			2)Configure GPIOC pins to be Output with High-speed 
			3)Make GPIOC to blink 

*/

#include "gpio.h"

unsigned long i=0;
int main(){
	//RCC_APB2ENR=(1<<2);	//Enable CLOCK for GPIOA Page - 146
	//RCC_APB2ENR = RCC_APB2ENR_IOPAEN;
	
	//Clock_Enable(GPIOA);
	//GPIOA_CRH =0x30000000; //Configure pins from 8...15 to be output pins
	//GPIOA->CRH = GPIO_CRH_MODE15;
	InitPort(GPIOA, GPIO_CRH_MODE15, true);
	
	while(1){
		
		//GPIOA_ODR=0x8000;   		//OUTPUT one to pin No. 15 pins of GPIOA
		//GPIOA->ODR = GPIO_ODR_ODR15;
		OutPort(GPIOA, GPIO_ODR_ODR15, false);
		//for(i=0;i<1000;i++);  //delay
		Delay(1000);
		//GPIOA_ODR=0x0000;  			//OUTPUT 0 to GPIOC pins
		//GPIOA->ODR = GPIO_ODR_ODRZERO;
		OutPort(GPIOA, GPIO_ODR_ODRZERO, true);
		//for(i=0;i<1000;i++);  //delay
		Delay(1000);

	}
	return 0;
	
}