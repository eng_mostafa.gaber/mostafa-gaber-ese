#define ADDR(x)     (*((unsigned long*)(x)))

// These registers define clocks, RCC (Reset and clock control)
#define RCC_BASE        0x40021000
#define RCC_APB2ENR     ADDR(RCC_BASE + 0x18)

#define PERIPH_BASE           (0x40000000) /*!< Peripheral base address in the alias region */

/*!< Peripheral memory map */
#define APB1PERIPH_BASE       PERIPH_BASE         		 //APB1 'BUS for Peripherals' base address
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000) //APB2 Peripheral base address
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)     
#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)  
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)  
#define GPIOC_BASE            (APB2PERIPH_BASE + 0x1000) 
#define GPIOD_BASE            (APB2PERIPH_BASE + 0x1400)

/*GPIOA Register  Address*/
#define GPIOA_CRL             (*((unsigned long*)(GPIOA_BASE + 0x00)))
#define GPIOA_CRH            	(*((unsigned long*)(GPIOA_BASE + 0x04)))
#define GPIOA_IDR             (*((unsigned long*)(GPIOA_BASE + 0x08)))
#define GPIOA_ODR             (*((unsigned long*)(GPIOA_BASE + 0x0C)))
#define GPIOA_BSRR    			  (*((unsigned long*)(GPIOA_BASE + 0x10)))
	

/*GPIOB Register  Address*/
#define GPIOB_CRL             (*((unsigned long*)(GPIOB_BASE + 0x00)))
#define GPIOB_CRH            	(*((unsigned long*)(GPIOB_BASE + 0x04)))
#define GPIOB_IDR             (*((unsigned long*)(GPIOB_BASE + 0x08)))
#define GPIOB_ODR             (*((unsigned long*)(GPIOB_BASE + 0x0C)))
#define GPIOB_BSRR    			  (*((unsigned long*)(GPIOB_BASE + 0x10)))
	
//#define GPIOC_BASE            (APB2PERIPH_BASE + 0x1000)
#define GPIOC_CRL             ADDR(GPIOC_BASE + 0x00)
#define GPIOC_CRH             (*((unsigned long*)(GPIOC_BASE + 0x04)))
#define GPIOC_IDR             ADDR(GPIOC_BASE + 0x08)
#define GPIOC_ODR             ADDR(GPIOC_BASE + 0x0C)
#define GPIOC_BSRR    			  ADDR(GPIOC_BASE + 0x10)


/*GPIOD Register  Address*/
#define GPIOD_CRL             (*((unsigned long*)(GPIOD_BASE + 0x00)))
#define GPIOD_CRH            	(*((unsigned long*)(GPIOD_BASE + 0x04)))
#define GPIOD_IDR             (*((unsigned long*)(GPIOD_BASE + 0x08)))
#define GPIOD_ODR             (*((unsigned long*)(GPIOD_BASE + 0x0C)))
#define GPIOD_BSRR    			  (*((unsigned long*)(GPIOD_BASE + 0x10)))
	