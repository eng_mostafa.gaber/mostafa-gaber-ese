#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	// Configuration
	// enable register page 146
	RCC->APB2ENR |= (1<<2) | (1<<4);
	// All pin to be output with
	GPIOA->CRL = 0x00000333;
	GPIOC->CRH = 0x04000000; // Pin 14 input
	int POS=0;
	while(1)
	{
		
		if(GPIOC->IDR & 0x4000) // Pin 14
		{
			POS++;
			if(POS>3){
				POS=1;
			}
		}
		switch(POS)
		{
			case 1:
				GPIOA->ODR |= (1<<0);	// set led 1 on
			  //for(int i=0; i<10000000;i++);		// delay
				GPIOA->ODR &= ~(1<<1);	// set led 2 off
				GPIOA->ODR &= ~(1<<2);// set led 3 off
				break;
			case 2:
				GPIOA->ODR &= ~(1<<0);	// set led 1 off
				GPIOA->ODR |= (1<<1);	// set led 2 on
			  //for(int i=0; i<10000000;i++);		// delay
				GPIOA->ODR &= ~(1<<2);	// set led 3 off
				break;
			case 3:
				GPIOA->ODR &= ~(1<<0);	// set led 1 off
				GPIOA->ODR &= ~(1<<1);	// set led 2 on
				GPIOA->ODR |= (1<<2);	// set led 3 off
			  //for(int i=0; i<10000000;i++);		// delay
				break;
		}
		for(int i=0; i<500000;i++);		// delay
		}
	
	return 0;
}