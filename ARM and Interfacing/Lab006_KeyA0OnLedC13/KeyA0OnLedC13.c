#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	// Configuration
	// enable register page 146
	RCC->APB2ENR |= (1<<2) | (1<<4);
	// All pin to be output with
	GPIOA->CRL = 0x00000004;
	GPIOC->CRH = 0x00300000;
	while(1)
	{
		//loop
		if(GPIOA->IDR & 0x0001)
		{
			GPIOC->ODR &= ~(1<<13);	// set complimant 1 (0) to pin 13
			for(int i=0; i<100000;i++);		// delay
		}
		else
		{
			GPIOC->ODR |= (1<<13); // set 1 to pin 13
			for(int i=0; i<100000;i++);		// delay
		}
	}
	return 0;
}